﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21FibIndexer
{
    class FibStore
    {
        public long this[int index]
        {
            get
            {
                if(index < 1)
                {
                    throw new ArgumentException("Fibs only exist for positive numbers");
                }
                long f1 = 1, f2 = 1;
                long fib = f1;
                for (int i = 1; i <= index; i++)
                {
                    if (i == 1 || i == 2)
                    {
                        fib = f1;
                    }
                    else if (i == 2)
                    {
                        fib = f2;
                    }
                    else
                    {
                        fib = f1 + f2;
                        f1 = f2;
                        f2 = fib;
                    }
                }
                return fib;
            }
            set
            {

            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                FibStore fs = new FibStore();
                Console.WriteLine(fs[1]);
                Console.WriteLine(fs[2]);
                Console.WriteLine(fs[3]);
                Console.WriteLine(fs[4]);
                Console.WriteLine(fs[55]);
                Console.WriteLine(fs[-4]);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine("Negative input: " + ex.Message);
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
