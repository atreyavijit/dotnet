﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day03People
{
    //class Person
    class Person
    {
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if(value > 150 || value < 0 )
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
    }

    //class Student
    class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            Program = program;
            GPA = gpa;
        }
        private string _program;
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Program  must be between 2 to 100 characters");
                }
                _program = value;
            }
        }
        private double _gpa;
        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value > 4.0 || value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("GPA must be between 0.0 to 4.0");
                }
                _gpa = value;
            }
        }
    }

    //class Teacher
    class Teacher : Person
    {
        public Teacher(string name, int age,string field, int yoe) : base(name, age)
        {
            Field = field;
            YOE = yoe;
        }
        private string _field;
        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Field must be between 2 to 100 characters");
                }
                _field = value;
            }
        }
        private int _yoe;
        public int YOE
        {
            get
            {
                return _yoe;
            }
            set
            {
                if (value > 100 || value < 0)
                {
                    throw new ArgumentOutOfRangeException("Years of Experience must be between 0 and 150");
                }
                _yoe = value;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //create a new person
                Person p = new Person("Jerry", 33);
                Console.WriteLine("Person: {0} is {1} y/o", p.Name, p.Age);

                //create a new student
                Student s = new Student("John", 23, "IPD", 3.25);
                Console.WriteLine("Student :{0} is {1} years old, studying {2} and current GPA is {3}", s.Name, s.Age, s.Program, s.GPA);

                //create a new teacher
                Teacher t = new Teacher("Russell", 48, "DotNet", 25);
                Console.WriteLine("Teacher :{0} is {1} years old, expert in {2} and with an experience of {3} years", t.Name, t.Age, t.Field, t.YOE);
            }
            catch(ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Exception: {0}", ex.Message);
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                Console.Write("Press any key to terminate");
                Console.ReadKey();
            }
        }
    }
}
