﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day21Delegates
{
    class Program
    {
        //step1: declare delegate type
        public delegate void LogMessageDelegate(string s);

        //step2: declare a field or property of delegate type
        static LogMessageDelegate Logger; //initially null when empty

        //step3: create one or more methods whose signatures match that of the delegate
        static void LogToConsole(string msg)
        {
            Console.WriteLine("MSG: " + msg);
        }

        static void Main(string[] args)
        {
            try
            {
                Logger += LogToConsole;
                Logger("This is Sparta!");
            }
            finally
            {
                Console.Write("Press any key to exit");
                Console.ReadKey();

            }
        }
    }
}
