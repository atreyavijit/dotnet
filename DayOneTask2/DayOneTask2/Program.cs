﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayOneTask2
{
    class Program
    {
        static int inputAge()
        {
            string ageStr;
            int age;
            Console.Write("Please provide your age: ");
            ageStr = Console.ReadLine();
            while (!int.TryParse(ageStr, out age) || age <= 0)
            {
                Console.Write("Invalid age!!! Please enter a positive integer for age: ");
                ageStr = Console.ReadLine();
            }
            return age;

        }
        static void Main(string[] args)
        {
            string name;
            int age;
            Console.Write("Please enter your name: ");
            name = Console.ReadLine();
            age = inputAge();
            Console.WriteLine("Hi {0}, you are {1} y/o.",name,age);
            if(age<18)
            {
                Console.WriteLine("You are not an adult yet.");
            }
            Console.Write("\nPress any key to end");
            Console.ReadKey();
        }
    }
}
