﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day08TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private char currentPlayer = 'O';
        private string finalStatus = "Draw";
        private Button[,] playButtons = new Button[3, 3];
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            playButtons[0, 0] = btn00;
            playButtons[0, 1] = btn01;
            playButtons[0, 2] = btn02;
            playButtons[1, 0] = btn10;
            playButtons[1, 1] = btn11;
            playButtons[1, 2] = btn12;
            playButtons[2, 0] = btn20;
            playButtons[2, 1] = btn21;
            playButtons[2, 2] = btn22;
            string errorMessage = "";
            if (tbPlayerO.Text == "")
            {
                errorMessage += String.Format("Name for Player0 is empty{0}", Environment.NewLine);
                tbPlayerO.Background = Brushes.Red;
            }
            if (tbPlayerX.Text == "")
            {
                errorMessage += String.Format("Name for PlayerX is empty{0}", Environment.NewLine);
                tbPlayerX.Background = Brushes.Red;
            }
            if (errorMessage != "")
            {
                MessageBox.Show(errorMessage);
                return;
            }
            foreach (Button button in playButtons)
            {
                button.IsEnabled = true;
            }
            tbPlayerO.IsEnabled = false;
            tbPlayerX.IsEnabled = false;
            lbPlayerO.Background = Brushes.Olive;
            lbPlayerX.Background = Brushes.White;
        }
        private void Bt_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if (btn.Content != null)
            {
                return;
            }
            if (currentPlayer == 'O')
            {
                btn.Content = "O";
                currentPlayer = 'X';
                lbPlayerX.Background = Brushes.Olive;
                lbPlayerO.Background = null;
            }
            else if (currentPlayer == 'X')
            {
                btn.Content = "X";
                currentPlayer = 'O';
                lbPlayerO.Background = Brushes.Olive;
                lbPlayerX.Background = null;
            }
            if(IsGameOver() == true)
            {
                if (finalStatus == "Draw")
                {
                    MessageBox.Show("Game Over!!! It's a draw");
                }
                else if(finalStatus == "Win")
                {
                    if(currentPlayer == 'X')
                    {
                        MessageBox.Show(String.Format("Game Over!!! " + tbPlayerO.Text + " has won"));
                    }
                    else if(currentPlayer == 'O')
                    {
                        MessageBox.Show(String.Format("Game Over!!! " + tbPlayerX.Text +" has won"));
                    }
                    else
                    {
                        MessageBox.Show("Internal Error");
                    }
                }
                else
                {
                    MessageBox.Show("Internal Error");
                }
                postGameActions();
            }
        }
        private bool IsGameOver()
        {
            if (
                (playButtons[0, 0].Content != null) && (playButtons[0, 0].Content == playButtons[0, 1].Content) && (playButtons[0, 0].Content == playButtons[0, 2].Content) ||
                (playButtons[1, 0].Content != null) && (playButtons[1, 0].Content == playButtons[1, 1].Content) && (playButtons[1, 0].Content == playButtons[1, 2].Content) ||
                (playButtons[2, 0].Content != null) && (playButtons[2, 0].Content == playButtons[2, 1].Content) && (playButtons[2, 0].Content == playButtons[2, 2].Content) ||
                (playButtons[0, 0].Content != null) && (playButtons[0, 0].Content == playButtons[1, 0].Content) && (playButtons[0, 0].Content == playButtons[2, 0].Content) ||
                (playButtons[0, 1].Content != null) && (playButtons[0, 1].Content == playButtons[1, 1].Content) && (playButtons[0, 1].Content == playButtons[2, 1].Content) ||
                (playButtons[0, 2].Content != null) && (playButtons[0, 2].Content == playButtons[1, 2].Content) && (playButtons[0, 2].Content == playButtons[2, 2].Content) ||
                (playButtons[0, 0].Content != null) && (playButtons[0, 0].Content == playButtons[1, 1].Content) && (playButtons[0, 0].Content == playButtons[2, 2].Content) ||
                (playButtons[0, 2].Content != null) && (playButtons[0, 2].Content == playButtons[1, 1].Content) && (playButtons[0, 2].Content == playButtons[2, 0].Content)
                )
            {
                finalStatus = "Win";
                return true;
            }
            else
            {
                foreach (Button btn in playButtons)
                {
                    if (btn.Content == null)
                    {
                        return false;
                    }
                }
                finalStatus = "Draw";
                return true;
            }
        }
        private void postGameActions()
        {
            foreach(Button btn in playButtons)
            {
                btn.Content = null;
                btn.IsEnabled = false;
            }
            currentPlayer = 'X';
            finalStatus = "Draw";
            tbPlayerO.IsEnabled = true;
            tbPlayerO.Text = "";
            lbPlayerO.Background = null;
            tbPlayerX.IsEnabled = true;
            tbPlayerX.Text = "";
            lbPlayerX.Background = null;
            //Btn_Click(null, null);
            //InitializeComponent();
        }
    }
}
