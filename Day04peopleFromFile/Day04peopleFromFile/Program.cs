﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04peopleFromFile
{
    //class Person
    class Person
    {
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value > 150 || value < 0)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Name: {0}, Age: {1}" ,Name, Age);
        }
    }
    // class Program -- with main function
    class Program
    {
        static List<Person> people = new List<Person>();
        static void Main(string[] args)
        {
            const string FILE_PATH = @"..\..\..\people.txt";
            try
            {
                string [] lines = File.ReadAllLines(FILE_PATH);
                foreach (string l in lines)
                {
                    try
                    {
                        string[] data = l.Split(';');
                        if (data.Length != 2)
                        {
                            throw new InvalidDataException("Line must have exactly two data )
                        }
                        string name = data[0];
                        int age = int.Parse(data[1]);
                        Person p = new Person(name, age);
                        people.Add(p);
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("{0}", ex.Message);
                    }
                }
                Console.WriteLine("The information about the persons from the file:");
                foreach (Person p in people)
                {
                    //Console.WriteLine("Name: {0}, Age: {1}", p.Name, p.Age);
                    Console.WriteLine(p);
                }
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
