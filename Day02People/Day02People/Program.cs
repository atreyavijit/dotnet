﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02People
{
    class Program
    {
        static void Main(string[] args)
        {
            String[] namesArray = new String[4];
            int[] agesArray = new int[4];
            int i, minAgePosition;
            string ageStr;
            try
            {
                Console.WriteLine("Provide names and ages of 4 persons");
                for (i = 0; i < 4; i++)
                {
                    Console.Write("Enter name of person #{0}: ", i + 1);
                    namesArray[i] = Console.ReadLine();
                    while (namesArray[i] == "")
                    {
                        Console.Write("Name for person #{0} is empty. Please enter a non empty name: ", i + 1);
                        namesArray[i] = Console.ReadLine();
                    }
                    Console.Write("Enter age of person #{0}: ", i + 1);
                    ageStr = Console.ReadLine();
                    while (ageStr == "" || !int.TryParse(ageStr, out agesArray[i]))
                    {
                        Console.Write("Invalid or empty age for person #{0}. Please enter valid age: ", i + 1);
                        ageStr = Console.ReadLine();
                    }
                }
                minAgePosition = 0;
                for (i = 1; i < 4; i++)
                {
                    if (agesArray[i] < agesArray[minAgePosition])
                    {
                        minAgePosition = i;
                    }
                }
                Console.WriteLine("Youngest person is {0} years and their name is {1}", agesArray[minAgePosition], namesArray[minAgePosition]);
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
