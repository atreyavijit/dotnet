﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayOneTask3
{
    class Program
    {
        static void Main(string[] args)
        {
            double [] numbers = new double[5];
            double min, max, sum = 0, average;
            double sumSqDev, stDev; //sum of squares of deviations about average and standard deviation
            string numStr;
            int count;
            Console.WriteLine("Enter any five floating point numbers: ");
            //loop for reading 5 numbers and finding their sum
            for (count = 0; count < 5; count++) 
            {
                Console.Write("Provide data number {0}: ", count + 1);
                numStr = Console.ReadLine();
                while (!double.TryParse(numStr, out numbers[count])) //asks for inputs until the user provides a valid float
                {
                    Console.Write("Invalid number format. Please provide floating point data: ");
                    numStr = Console.ReadLine();
                }
                sum += numbers[count];
            }
            average = sum / 5;
            //Loop for finding min and max
            min = max = numbers[0]; //initialize both min and max to the first element of the array
            for (count = 1; count < 5; count++) //loop started at 1
            {
                min = (min > numbers[count]) ? numbers[count] : min;
                max = (max < numbers[count]) ? numbers[count] : max;
            }

            //calculating standard deviation
            sumSqDev = 0; //initialize sum of squares of deviations about the average
            for(count = 0; count < 5; count++)
            {
                sumSqDev += (float)(Math.Pow(numbers[count] - average, 2));
            }
            stDev = (float)(Math.Sqrt(sumSqDev/4));
            Console.WriteLine("\nThe statistics of the dataset you provided are as follows:");
            Console.WriteLine("Min = {0}\nMax = {1}", min, max);
            Console.WriteLine("Sum = {0}\nAverage = {1}", sum, average);
            Console.WriteLine("Standard Deviation = {0}", stDev);
            Console.Write("\nPress any key to end");
            Console.ReadKey();
        }
    }
}
