﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14FirstDB
{
    class Program
    {
        //database connection string
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896170\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";

        static SqlConnection conn;

        static void Main(string[] args)
        {
            try
            {
                conn = new SqlConnection(DbConnectionString);
                conn.Open();
                int id;
                string name;
                int age;

                // insert example
                try
                {
                    Console.Write("Enter name of person: "); name = Console.ReadLine();
                    SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);
                    cmdInsert.Parameters.AddWithValue("Name", name);
                    cmdInsert.Parameters.AddWithValue("Age", new Random().Next(100));
                    //cmdInsert.ExecuteNonQuery();
                    int insertId = (int)cmdInsert.ExecuteScalar();
                    Console.WriteLine("Record inserted successfully with id = {0}",insertId);
                }
                catch(Exception ex)
                {
                    if(ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else
                    {
                        throw ex;
                    }
                }
                
                
                //select example
                try
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People", conn);
                    using (SqlDataReader reader = cmdSelect.ExecuteReader())
                    {
                        while(reader.Read())
                        {
                            id = (int)reader[0];
                            name = (string)reader[1];
                            age = (int)reader[2];
                            Console.WriteLine("Person({0}): {1} is {2} years old", id, name, age);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else
                    {
                        throw ex;
                    }
                }

                // update example
                try
                {
                    Console.Write("Enter id to update: ");
                    id = int.Parse(Console.ReadLine());
                    Console.Write("Enter a new name: ");
                    name = Console.ReadLine();
                    SqlCommand cmdUpdate = new SqlCommand("UPDATE People SET Name = @Name WHERE Id= @Id", conn);
                    cmdUpdate.Parameters.AddWithValue("Id", id);
                    cmdUpdate.Parameters.AddWithValue("Name", name);
                    int rowsAffected = cmdUpdate.ExecuteNonQuery();
                    if(rowsAffected >= 1)
                    {
                        Console.WriteLine("Record deleted successfully");
                    }
                    else
                    {
                        Console.WriteLine("No record found");
                    }
                }
                catch (Exception ex)
                {
                    if (ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else if (ex is FormatException)
                    {
                        Console.WriteLine("Data format invalid");
                    }
                    else
                    {
                        throw ex;
                    }
                }
                /*Console.WriteLine("Enter a record id to update: ");
                if (!int.TryParse(Console.ReadLine(), out id))
                {
                    Console.WriteLine("Not a valid id");
                }
                else
                {
                    SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People WHERE Id = @id", conn);
                    cmdSelect.Parameters.AddWithValue("Id", id);
                    using (SqlDataReader reader = cmdSelect.ExecuteReader())
                    {
                        id = (int)reader[0];
                        name = (string)reader[1];
                        age = (int)reader[2];
                        Console.WriteLine("Person({0}): {1} is {2} years old", id, name, age);
                    }
                    Console.Write("Enter a new name: ");
                    name = Console.ReadLine();
                    Console.Write("Enter a new age: ");
                    if (!int.TryParse(Console.ReadLine(), out age))
                    {
                        Console.WriteLine("Invalid Age. Must be integer");
                    }
                    else
                    {
                        SqlCommand cmdUpdate = new SqlCommand("UPDATE People set Name = @name, Age = @age WHERE Id=@id", conn);
                        cmdUpdate.Parameters.AddWithValue("name", name);
                        cmdUpdate.Parameters.AddWithValue("age", age);
                        cmdUpdate.Parameters.AddWithValue("id", id);
                        cmdUpdate.ExecuteNonQuery();
                        Console.WriteLine("Record updated successfully");
                    }
                }*/

                // delete example
                try
                {
                    Console.Write("Enter an id to delete: ");
                    id = int.Parse(Console.ReadLine());
                    SqlCommand cmdDelete = new SqlCommand("DELETE FROM People WHERE Id= @Id", conn);
                    cmdDelete.Parameters.AddWithValue("Id", id);
                    int rowsAffected = cmdDelete.ExecuteNonQuery();
                    if (rowsAffected >= 1)
                    {
                        Console.WriteLine("Record deleted successfully");
                    }
                    else
                    {
                        Console.WriteLine("No record found");
                    }
                }
                catch (Exception ex)
                {
                    if (ex is InvalidCastException || ex is SqlException)
                    {
                        Console.WriteLine("Error executing query: " + ex.Message);
                    }
                    else if (ex is FormatException)
                    {
                        Console.WriteLine("Data format invalid");
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            catch(Exception ex)
            {
                if(ex is SqlException || ex is InvalidOperationException)
                {
                    Console.WriteLine("Error conneting database: " + ex.Message);
                }
                else
                {
                    throw ex;
                }
            }
            finally
            {
                Console.Write("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
