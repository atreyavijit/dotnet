﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatMult
{
    class Program
    {
        //reading integers
        static int readInput()
        {
            String strData;
            int data;
            strData = Console.ReadLine();
            while(!int.TryParse(strData, out data))
            {
                Console.Write("Not a valid data. Please provide an integer: ");
                strData = Console.ReadLine();
            }
            return data;
        }
        //reading data for matrix
        static void readMatrix(int[,] matrix, int row, int column)
        {
            int i, j;
            for(i = 0; i < row; i++)
            {
                for(j = 0; j < column; j++)
                {
                    Console.Write("Data number [{0},{1}]: ", i, j);
                    matrix[i, j] = readInput();
                }
            }
        }
        //displaying matrixes in matrix layout
        static void displayMatrix(int[,] Matrix, int row, int column)
        {
            int i, j;
            for (i = 0; i < row; i++)
            {
                for (j = 0; j < column; j++)
                {
                    Console.Write("{0}   ", Matrix[i, j]);
                }
                Console.Write("\n");
            }
        }
        //multiplying matrixes
        static void multiplyMatrix(int[,] X, int[,] Y, int[,] Z, int row , int rc, int column)
        {
            int i, j, k;
            int tempSum;
            for(i = 0; i < row; i++)
            {
                for(j = 0; j < column; j++)
                {
                    tempSum = 0;
                    for(k = 0; k < rc; k++)
                    {
                        tempSum += X[i, k] * Y[k, j];
                    }
                    Z[i, j] = tempSum;
                }
            }
        }
        static void Main(string[] args)
        {
            int rowFirst, colFirst, rowSecond, colSecond;
            try
            {
                Console.Write("Please enter rows of first matrix: ");
                rowFirst = readInput();
                Console.Write("Please enter columns of first matrix: ");
                colFirst = readInput();
                Console.Write("Please enter rows of second matrix: ");
                rowSecond = readInput();
                Console.Write("Please enter columns of second matrix: ");
                colSecond = readInput();
                int[,] First = new int[rowFirst, colFirst];
                int[,] Second = new int[rowSecond, colSecond];
                if(colFirst == rowSecond)
                {
                    int[,] Result = new int[rowFirst, colSecond];
                    Console.WriteLine("\nMatrix Multiplication is possible.\nYour result matrix will have {0} rows and {1} columns",rowFirst,colSecond);
                    Console.WriteLine("\nPlease provide data for first matrix:");
                    readMatrix(First, rowFirst, colFirst);
                    Console.WriteLine("\nPlease provide data for second matrix:");
                    readMatrix(Second, rowSecond, colSecond);
                    Console.WriteLine("\nYour First Matrix is: ");
                    displayMatrix(First,rowFirst,colFirst);
                    Console.WriteLine("\nYour Second Matrix is: ");
                    displayMatrix(Second, rowSecond, colSecond);
                    multiplyMatrix(First, Second, Result, rowFirst, colFirst, colSecond);
                    Console.WriteLine("\nYour Result Matrix is: ");
                    displayMatrix(Result, rowFirst, colSecond);
                }
				else
				{
					Console.WriteLine("You can't perform matrix multiplication of size {0} X {1} with size {2} X {3} but the reverse may be possible.",rowFirst,colFirst,rowSecond,colSecond );
				}
					
            }
            finally
            {
                Console.Write("Press any key to end the program: ");
                Console.ReadKey();
            }
        }
    }
}
