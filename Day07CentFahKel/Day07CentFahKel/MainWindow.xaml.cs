﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07CentFahKel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void TbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            Recalculate();
        }

        private void Rb_Clicked(object sender, RoutedEventArgs e)
        {
            Recalculate();
        }
        private void Recalculate()
        {
            //STEP1: read input and validate
            string inputStr = tbInput.Text;
            double inputValue;
            if (inputStr == "")
            {
                return;
            }
            //STEP convert and display the results
            if (!double.TryParse(inputStr, out inputValue))
            {
                MessageBox.Show("Please enter a valid number");
                tbInput.Text = "";
                return;
            }
            if (rbInputC.IsChecked == true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} C", inputValue);
                }
                else if (rbOutputF.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} F", 9 * inputValue / 5 + 32);
                }
                else if (rbOutputK.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} K", inputValue + 273.15);
                }
                else
                {
                    MessageBox.Show("Internal Error. Unknown scale");
                    return;
                }
            }
            else if (rbInputF.IsChecked == true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} C", 5 * (inputValue - 32) / 9);
                }
                else if (rbOutputF.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} F", inputValue);
                }
                else if (rbOutputK.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} K", 5 * (inputValue - 32) / 9 + 273.15);
                }
                else
                {
                    MessageBox.Show("Internal Error. Unknown scale");
                    return;
                }
            }
            else if (rbInputK.IsChecked == true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} C", inputValue - 273.15);
                }
                else if (rbOutputF.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} F", 9 * (inputValue - 273.15) / 5 + 32);
                }
                else if (rbOutputK.IsChecked == true)
                {
                    tbOutput.Text = String.Format("{0:0.##} K", inputValue);
                }
                else
                {
                    MessageBox.Show("Internal Error. Unknown scale");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Internal Error. Unknown scale");
                return;
            }
        }
    }
}
