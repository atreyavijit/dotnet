﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz1Triple
{
    class Program
    {
        //checking for fibonacci
        static int fibo(int n)
        {
            if (n == 1)
                return 0;
            else if(n==2)
            {
                return 1;
            }
            else
            {
                return fibo(n - 1) + fibo(n - 2);
            }
        }
        static void FibChecker(int numberToCheck)
        {
            int fibNumber;
            for(int i = 1; ;i++)
            {
                fibNumber = fibo(i);
                if (fibNumber > numberToCheck)
                {
                    Console.WriteLine("No {0} is not a fibonacci number. The next fibonacci number is {1}", numberToCheck, fibNumber);
                    break;
                }
                else if (fibNumber == numberToCheck)
                {
                    Console.WriteLine("Yes {0} is a fibonacci number. The next fibonacci number is {1}", numberToCheck, fibo(i+1));
                    break;
                }

            }
        }

        //read matrix
        static int ReadMatrix(int[,] Matrix, int row, int column)
        {
            for (int i = 0; i < row; i++)
            {
                String[] currentRow = (Console.ReadLine()).Split(',');
                if (currentRow.Length != column)
                {
                    Console.WriteLine("Inappropriate number of data.");
                    return 1;
                }
                for (int j = 0; j < column; j++)
                {
                    if (!int.TryParse(currentRow[j], out Matrix[i, j]))
                    {
                        Console.WriteLine("Data format mismatch.");
                        return 1;
                    }
                }
            }
            return 0;
        }

        //matrix multiplier
        static void MatrixMultiplier(int[,] X, int[,] Y, int[,] Z)
        {
            int i, j, k;
            int tempSum;
            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 2; j++)
                {
                    tempSum = 0;
                    for (k = 0; k < 3; k++)
                    {
                        tempSum += X[i, k] * Y[k, j];
                    }
                    Z[i, j] = tempSum;
                }
            }
        }

        //display matrix
        static void DisplayMatrix(int[,] Matrix, int row, int column)
        {
            int i, j;
            for (i = 0; i < row; i++)
            {
                for (j = 0; j < column; j++)
                {
                    Console.Write("{0}{1}", Matrix[i, j],(j==column-1)?"":",");
                }
                Console.Write("\n");
            }
        }

        static void Main(string[] args)
        {
            int NumberToChcek;
            
            try
            {
                Console.WriteLine("*******************************************************************************************");
                Console.WriteLine("The First part of the program where you will check if an integer is fibonacci number or not");
                Console.WriteLine("*******************************************************************************************");
                Console.Write("Please enter a prositive integer to check if it is a fibonacci number: ");
                if(!int.TryParse(Console.ReadLine(),out NumberToChcek) || NumberToChcek <0)
                {
                    Console.WriteLine("The number is neagtive or in invalid format.");
                    return;
                }
                FibChecker(NumberToChcek);
                Console.WriteLine();
                Console.WriteLine("*******************************************************************");
                Console.WriteLine("The Second part of the program where you will multiply two matrixes");
                Console.WriteLine("*******************************************************************");
                int[,] FirstMatrix = new int[2, 3];
                int[,] SecondMatrix = new int[3, 2];
                int[,] ResultMatrix = new int[2, 2];

                //read first matrix
                Console.WriteLine("Please provide data (2 rows of 3 data each) for first matrix, data separated by comma and one row separated by line:");
                if(ReadMatrix(FirstMatrix, 2, 3) == 1)
                    return;

                //read second matrix 
                Console.WriteLine("Please provide data (3 rows of 2 data each) for first matrix, data separated by comma and one row separated by line:");
                if (ReadMatrix(SecondMatrix, 3, 2) == 1)
                    return;

                //Console.WriteLine("\nYour First Matrix is: ");
                //DisplayMatrix(FirstMatrix, 2, 3);
                //Console.WriteLine("\nYour Second Matrix is: ");
                //DisplayMatrix(SecondMatrix, 3, 2);

                //multiply matrixes
                MatrixMultiplier(FirstMatrix, SecondMatrix, ResultMatrix);

                //display the result matrix
                Console.WriteLine("\nYour Result Matrix is: ");
                DisplayMatrix(ResultMatrix, 2, 2);
            }
            finally
            {
                Console.Write("Press any key to terminate.");
                Console.ReadKey();
            }
            
        }
    }
}
