﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz3Calc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string currentInput = "first";
        double firstValue = 0.0;
        double secondValue = 0.0;
        double result = 0.0;
        string operation = "";
        bool isDataWrittenToFile = false;
        const string FILE_PATH = @"..\..\..\record.txt";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Value_Click(object sender, RoutedEventArgs e)
        {
            Button valueButton = (Button)sender;
            if (currentInput == "first")
            {
                tbFirstValue.Text += valueButton.Content.ToString();
            }
            if (currentInput == "second")
            {
                tbSecondValue.Text += valueButton.Content.ToString();
            }
        }

        private void Btn_PlusMinus_Click(object sender, RoutedEventArgs e)
        {
            double textBoxNumber = 0.0;
            if(currentInput == "first") 
            {
                if (!double.TryParse(tbFirstValue.Text, out textBoxNumber))
                {
                    MessageBox.Show("First Value must be a valid number");
                    return;
                }
                tbFirstValue.Text = String.Format("{0}", -1 * textBoxNumber);
            }
            else if (currentInput == "second")
            {
                if (!double.TryParse(tbSecondValue.Text, out textBoxNumber))
                {
                    MessageBox.Show("Second Value must be a valid number");
                    return;
                }
                tbSecondValue.Text = String.Format("{0}", -1 * textBoxNumber);
            }
            else
            {
                MessageBox.Show("You can't change the sign of the result");
            }
        }

        private void Btn_Reset_Click(object sender, RoutedEventArgs e)
        {
            currentInput = "first";
            firstValue = 0.0;
            secondValue = 0.0;
            result = 0.0;
            operation = "";
            tbFirstValue.Text = "";
            tbFirstValue.IsEnabled = true;
            lbFirstValue.Background = Brushes.Yellow;
            lbOperationSymbol.Content = null;
            tbSecondValue.Text = "";
            tbSecondValue.IsEnabled = false;
            lbSecondValue.Background = null;
            tbResult.Text = "";
            tbResult.IsEnabled = false;
            lbResult.Background = null;
            isDataWrittenToFile = false;
        }

        private void Btn_Operator_Click(object sender, RoutedEventArgs e)
        {
            Button operationButton = (Button)sender;
            if (!double.TryParse(tbFirstValue.Text, out firstValue))
            {
                MessageBox.Show("First Value must be a valid number");
                return;
            }
            tbFirstValue.IsEnabled = false;
            lbFirstValue.Background = null;
            if (lbOperationSymbol.Content == null)
            {
                lbOperationSymbol.Content = operationButton.Content;
            }
            operation = operationButton.Content.ToString();
            tbSecondValue.IsEnabled = true;
            lbSecondValue.Background = Brushes.Yellow;
            tbFirstValue.IsEnabled = false;
            lbFirstValue.Background = null;
            currentInput = "second";
        }

        private void Btn_Equals_Click(object sender, RoutedEventArgs e)
        {
            if(tbFirstValue.Text == "")
            {
                Btn_Operator_Click(null,null);
                return;
            }
            if (!double.TryParse(tbSecondValue.Text, out secondValue))
            {
                MessageBox.Show("Second Value must be a valid number");
                return;
            }
            switch (operation)
            {
                case "+":
                    result = firstValue + secondValue;
                    break;
                case "-":
                    result = firstValue - secondValue;
                    break;
                case "*":
                    result = firstValue * secondValue;
                    break;
                case "/":
                    if (secondValue == 0)
                    {
                        MessageBox.Show("Second Value can't be zero for division");
                        return;
                    }
                    result = firstValue / secondValue;
                    break;
                default:
                    MessageBox.Show("Internal Error Occurred");
                    return;
            }
            tbSecondValue.IsEnabled = false;
            lbSecondValue.Background = null;
            tbResult.IsEnabled = true;
            lbResult.Background = Brushes.Yellow;
            tbFirstValue.IsEnabled = false;
            lbFirstValue.Background = null;
            tbResult.Text = String.Format("{0}", result);
            currentInput = "";
            try
            {
                if (isDataWrittenToFile == false)
                {
                    File.AppendAllText(FILE_PATH, String.Format("{0:0.########}{1}{2:0.########}={3:0.########}{4}", firstValue, operation, secondValue, result, Environment.NewLine));
                    isDataWrittenToFile = true;
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show("Error appending to file: " + ex.Message);
            }
        }
    }
}
