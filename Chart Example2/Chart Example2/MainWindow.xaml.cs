﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using Wpf.CartesianChart.PointShapeLine;

namespace Chart_Example2
{
    /// <summary>
    /// </summary>
    public partial class MainWindow : Window
    {
        List<LineSeries> lines = new List<LineSeries>();
        List<string> graphNames = new List<string>();
        public MainWindow()
        {
            InitializeComponent();
            LineSeries line2 = new LineSeries
            {
                Title = "Twenties",
                Values = new ChartValues<double> { 22, 27, 24, 22, 27, 22, 27, 24, 22, 27, 22, 26 }
            };
            LineSeries line3 = new LineSeries
            {
                Title = "Thirties",
                Values = new ChartValues<double> { 32, 37, 34, 32, 37, 32, 37, 34, 32, 37, 32, 36 }
            };
            LineSeries line4 = new LineSeries
            {
                Title = "Forties",
                Values = new ChartValues<double> { 42, 47, 44, 42, 47, 42, 47, 44, 42, 47, 42, 46 }
            };
            LineSeries line0 = new LineSeries
            {
                Title = "Singles",
                Values = new ChartValues<double> { 2, 7, 4, 2, 7, 2, 7, 4, 2, 7, 2, 6 }
            };
            LineSeries line1 = new LineSeries
            {
                Title = "Tens",
                Values = new ChartValues<double> { 12, 17, 14, 12, 17, 12, 17, 14, 12, 17, 12, 16 }
            };
            lines.Add(line0);
            lines.Add(line1);
            lines.Add(line2);
            lines.Add(line3);
            lines.Add(line4);

            //assign different colors to the lines
            Random rnd = new Random();
            for(int i = 0; i < lines.Count;i++)
            {
                lines[i].Stroke = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(0, 255), (byte)rnd.Next(0, 255), (byte) rnd.Next(0, 255)));
                lines[i].Fill = Brushes.Transparent;
            }
            foreach(LineSeries line in lines)
            {
                graphNames.Add(line.Title);
            }
            lvLines.ItemsSource = graphNames;
        }

        private void LvLines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PointShapeLineExample.clearChart();
            for (int i= 0; i < graphNames.Count;i++)
            {
                if(lvLines.SelectedItems.Contains(graphNames[i]))
                {
                    PointShapeLineExample.addChartToSeriesColllection(lines[i]);
                }
            }
        }
    }
}
