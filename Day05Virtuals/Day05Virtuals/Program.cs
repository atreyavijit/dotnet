﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05Virtuals
{
    class A
    {
        public void Foo() { Console.WriteLine("A::Foo()"); }
    }

    class B : A
    {
        public void Foo() { Console.WriteLine("B::Foo()"); }
    }
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}
