﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day16TodosDB
{
    class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896170\Documents\TodosDB.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        //select todos from table
        public List<Todo> GetAllTodos(string orderBy = "Id")
        {
            List<Todo> list = new List<Todo>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Todos ORDER BY " + orderBy, conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string task = (string)reader[1];
                    DateTime dueDate = (DateTime)reader[2];
                    TaskStatus isDone = (TaskStatus)Enum.Parse(typeof(TaskStatus), (string)reader[3]);
                    list.Add(new Todo() { Id = id, Task = task, DueDate = dueDate, IsDone = isDone });
                }
            }
            return list;
        }

        //insert todo into table
        public int AddTodo(Todo todo)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Todos(Task, DueDate, IsDone) OUTPUT INSERTED.ID VALUES (@Task, @DueDate, @IsDone)",conn);
            cmdInsert.Parameters.AddWithValue("Task", todo.Task);
            cmdInsert.Parameters.AddWithValue("DueDate",todo.DueDate);
            cmdInsert.Parameters.AddWithValue("IsDone", todo.IsDone.ToString());
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }

        //update todo
        public bool UpdateTodo(Todo todo)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE Todos SET Task = @Task, DueDate = @DueaDate IsDone = @IsDone WHERE Id = @Id", conn);
            cmdUpdate.Parameters.AddWithValue("Id", todo.Id);
            cmdUpdate.Parameters.AddWithValue("Task", todo.Task);
            cmdUpdate.Parameters.AddWithValue("DueDate", todo.DueDate);
            cmdUpdate.Parameters.AddWithValue("IsDone", todo.IsDone);
            int affectedRows = cmdUpdate.ExecuteNonQuery();
            return affectedRows > 0;
        }

        //delete todo
        public bool DeleteTodo(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Todos WHERE Id = @Id",conn);
            cmdDelete.Parameters.AddWithValue("Id", id);
            int affectedRows = cmdDelete.ExecuteNonQuery();
            return affectedRows > 0;
        }
    }
}
