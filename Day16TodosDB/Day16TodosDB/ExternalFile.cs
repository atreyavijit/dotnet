﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;

namespace Day16TodosDB
{
    class ExternalFile
    {
        public List<Todo> ReadDataFromFile(string fileName)
        {
            List<Todo> todoListFromFile = new List<Todo>();
            try
            {
                string[] todoLines = File.ReadAllLines(fileName);
                string message = "";
                foreach (string todo in todoLines)
                {
                    string[] data = todo.Split(';');
                    if (data.Length != 3)
                    {
                        message += string.Format("Corrupted line. Must have exactly three data: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (!Regex.Match(data[0], @"^[^;]{1,100}$").Success)
                    {
                        message += string.Format("Corrupted line. Task must be 1 to 100 characters, without semicolon : {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (!Regex.Match(data[1], @"^[0-9]{4}[-][0-9]{1,2}[-][0-9]{1,2}$").Success)
                    {
                        message += string.Format("Corrupted line. Date Format Invalid: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (data[2] != "Pending" && data[2] != "Done")
                    {
                        message += string.Format("Corrupted line. Status must be either Pending or Done: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (!DateTime.TryParse(data[1], out DateTime dueDate))
                    {
                        message += string.Format("Corrupted line. Date Format Invalid: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    string task = data[0];
                    TaskStatus isDone;
                    if (data[2] == "Pending")
                    {
                        isDone = TaskStatus.Done;
                    }
                    else
                    {
                        isDone = TaskStatus.Pending;
                    }
                    try
                    {
                        Todo td = new Todo()
                        {
                            Task = task,
                            DueDate = dueDate,
                            IsDone = isDone
                        };
                        todoListFromFile.Add(td);
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        message += string.Format("Corrupted line: {0}. {1}{2}", todo, ex.Message, Environment.NewLine);
                    }
                }
                if (message != "")
                {
                    MessageBox.Show(message);
                }  
            }
            catch (FileNotFoundException)
            {
                //do nothing
                //its okay to start this program without the file
                //you can read data which will ultimately be written to file at the end
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: Can't read from the file: {0}", ex.Message);
            }
            return todoListFromFile;
        }
        public void WriteDataToFile(string fileName, List<Todo> todoList)
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] todoLines = new String[todoList.Count];
                int i = 0;
                foreach (Todo td in todoList)
                {
                    string date = String.Format("{0}-{1}-{2}", td.DueDate.Year, td.DueDate.Month, td.DueDate.Day);
                    string status = td.IsDone == TaskStatus.Done ? "Done" : "Pending";
                    todoLines[i++] = String.Format("{0};{1};{2}", td.Task, date, status);
                }
                File.WriteAllLines(fileName, todoLines);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: failed to save data to file: {0}", ex.Message);
            }
        }
    }
}
