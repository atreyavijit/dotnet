﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int count;
            string countStr;
            Random rand = new Random();
            List<int> randomNumbers = new List<int>();
            try
            {
                Console.Write("How many random number do you want between -100 and +100: ");
                countStr = Console.ReadLine();
                while (!int.TryParse(countStr, out count))
                {
                    Console.Write("Invalid Count. Please enter an integer value: ");
                    countStr = Console.ReadLine();
                }
                for(int i = 0; i < count; i++)
                {
                    randomNumbers.Add(rand.Next(-100,+101));
                }
                Console.WriteLine("\nHere is a list of all random numbers that are less or equal to 0:");
                foreach(int r in randomNumbers)
                {
                    if(r<=0)
                    {
                        Console.WriteLine(r);
                    }
                }
            }
            finally
            {
                Console.Write("Press any key to continue ...");
                Console.ReadKey();
            }
        }
    }
}
