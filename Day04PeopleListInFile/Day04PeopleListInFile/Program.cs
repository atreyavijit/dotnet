﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day04PeopleListInFile
{
    class Person
    {
        public Person(string name, int age, string city)
        {
            Name = name;
            Age = age;
            City = city;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Name must be between 2 to 100 characters long");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("City must be between 2 to 100 characters long");
                }
                _city = value;
            }
        }
        public override string ToString()
        {
            return string.Format("{0} is {1} from {2}", Name, Age, City);
        }
    }
    class Program
    {
        const string FILE_PATH = @"..\..\..\people.txt";
        static List<Person> people = new List<Person>();

        //reading data from the file
        static void ReadAddPeopleFromFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(FILE_PATH);
                foreach (string l in lines)
                {
                    string[] data = l.Split(';');
                    if (data.Length != 3)
                    {
                        Console.WriteLine("Corrupted line. Must have exactly three data: {0}",l);
                        continue;
                    }
                    string name = data[0];
                    int age;
                    if(!int.TryParse(data[1],out age))
                    {
                        Console.WriteLine("Corrupted line. Age must be integer: {0}", l);
                        continue;
                    }
                    string city = data[2];
                    try
                    {
                        Person p = new Person(name, age, city);
                        people.Add(p);
                    }
                    catch(ArgumentOutOfRangeException ex)
                    {
                        Console.WriteLine("Corrupted line: {0}\n{1}", l, ex.Message);
                    }
                }
            }
            catch(FileNotFoundException)
            {
                //do nothing
                //its okay to start this program without the file
                //you can read data which will ultimately be written to file at the end
            }
            catch(IOException ex)
            {
                Console.WriteLine("Error: Can't read from the file: {0}",ex.Message);
            }
        }

        //writing the final data to the file
        static void SaveAllPeopleToFile()
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] FinalData = new String[people.Count];
                int i = 0;
                foreach (Person p in people)
                {
                    FinalData[i++] = String.Format("{0};{1};{2}", p.Name, p.Age, p.City);
                }
                File.WriteAllLines(FILE_PATH, FinalData);
            }
            catch(IOException ex)
            {
                Console.WriteLine("Error: failed to save data to file" + ex.Message);
            }
        }

        //add information of a new person
        static void AddPersonInfo()
        {
            try
            {
                string name;
                int age;
                string city;
                Console.WriteLine();
                Console.WriteLine("Adding a Person");
                Console.Write("Enter Name: ");
                name = Console.ReadLine();
                Console.Write("Enter Age: ");
                int.TryParse(Console.ReadLine(), out age);
                Console.Write("Enter City: ");
                city = Console.ReadLine();
                people.Add(new Person(name, age, city));
                Console.WriteLine("Person Added");
                Console.WriteLine();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            catch(FormatException ex)
            {
                Console.WriteLine(ex.Message + " Error: Age must be an integer");
            }
        }

        //listing information of all persons
        static void ListAllPersonsInfo()
        {
            Console.WriteLine();
            Console.WriteLine("Listing all Persons.");
            foreach (Person p in people)
            {
                Console.WriteLine(p);
            }
            Console.WriteLine();
        }

        //finding a person by name
        static void FindPersonByName()
        {
            string searchStr;
            Console.WriteLine();
            Console.Write("Enter partial person name: ");
            searchStr = Console.ReadLine();
            Console.WriteLine("Matches found:");
            foreach (Person p in people)
            {
                if (FindSubString(p.Name, searchStr) == true)
                {
                    Console.WriteLine(p);
                }
            }
            Console.WriteLine();
        }

        //finding all persons younger than
        static void FindPersonYoungerThan()
        {
            int maxAge;
            Console.WriteLine();
            Console.Write("Enter maximum age: ");
            if (!int.TryParse(Console.ReadLine(), out maxAge))
            {
                Console.WriteLine("Invalid age");
                return;
            }
            Console.WriteLine("Matches found:");
            foreach (Person p in people)
            {
                if (p.Age <= maxAge)
                {
                    Console.WriteLine(p);
                }
            }
            Console.WriteLine();
        }

        //display menu
        static int ReadUserChoice()
        {
            int choice;
            while (true)
            {
                Console.WriteLine("***SELECT YOUR CHOICE***");
                Console.WriteLine("1. Add Person's Info");
                Console.WriteLine("2. List Person's Info");
                Console.WriteLine("3. Find a Person by Name");
                Console.WriteLine("4. Find All Persons Younger than Age");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid Choice\n");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return choice;
        }

        //search a string in another string
        static bool FindSubString(string name, string search)
        {
            int i, j, match;
            bool isFound = false;
            for (i = 0; i < name.Length; i++)
            {
                match = 1;
                for (j = 0; j < search.Length; j++)
                {
                    if (i + j >= name.Length || name[i + j] != search[j])
                    {
                        match = 0;
                        break;
                    }
                }
                if (match == 1)
                {
                    isFound = true;
                    break;
                }
            }
            return isFound;
        }

        //main function
        static void Main(string[] args)
        {
            try
            {
                int choice;
                ReadAddPeopleFromFile();
                do
                {
                    choice = ReadUserChoice();
                    switch (choice)
                    {
                        case 1:
                            AddPersonInfo();
                            break;
                        case 2:
                            ListAllPersonsInfo();
                            break;
                        case 3:
                            FindPersonByName();
                            break;
                        case 4:
                            FindPersonYoungerThan();
                            break;
                        case 0:
                            Console.WriteLine("Good Bye");
                            break;
                        default:
                            Console.WriteLine("Invalid Choice\n");
                            break;
                    }
                } while (choice != 0);
                SaveAllPeopleToFile();
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
