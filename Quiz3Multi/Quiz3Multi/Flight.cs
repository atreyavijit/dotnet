﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Flight
    {
        public override string ToString()
        {
            if (FromAirport != null && ToAirport != null)
            {
                return String.Format("Flight Id: {0}; Flight Number {1}; From Airport: {2} (Airport Id: {3}); To Airport: {4} (Airport Id: {5})", Id, Number, FromAirport.Code,FromAirport.Id,ToAirport.Code,ToAirport.Id);
            }
            else if (FromAirport == null)
            {
                return String.Format("Flight Id: {0}; Flight Number {1}; From Airport: Not Assigned; To Airport: {2} (Airport Id: {3})", Id, Number, ToAirport.Code, ToAirport.Id);
            }
            else if (FromAirport == null)
            {
                return String.Format("Flight Id: {0}; Flight Number {1}; From Airport: {2} (Airport Id: {3}); To Airport: Not Assigned", Id, Number, FromAirport.Code, FromAirport.Id);
            }
            else
            {
                return String.Format("Flight Id: {0}; Flight Number {1}; From Airport: Not Assigned; To Airport: Not Assigned", Id, Number);
            }
        }

        public int Id { get; set; }

        [MaxLength(5)]
        public string Number { get; set; } // two letters followed by two or three digits, e.g. UA234 

        public virtual Airport FromAirport { get; set; }
        public virtual Airport ToAirport { get; set; }

        
    }
}
