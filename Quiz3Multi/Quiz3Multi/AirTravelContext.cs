﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    class AirTravelContext : DbContext
    {
        public AirTravelContext() : base()
        {

        }
        public virtual DbSet<Flight> Flights { get; set; }
        public virtual DbSet<Airport> Airports { get; set; }
        public virtual DbSet<LogMessage> LogMessages { get; set; }
    }
}
