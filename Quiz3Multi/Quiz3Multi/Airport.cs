﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class Airport
    {
        public Airport()
        {
            FlightsList = new HashSet<Flight>();
        }
        public int Id { get; set; }

        [MaxLength(3)]
        public string Code { get; set; } // 3 uppercase letters

        [MaxLength(50)]
        public string City { get; set; } // 0-50 letters

        public virtual ICollection<Flight> FlightsList { get; set; }

        public override string ToString()
        {
            string flightsHosted = "";
            if (FlightsList.Count != 0)
            {
                foreach (Flight flight in FlightsList)
                {
                    flightsHosted += string.Format("Flight Code: {0} (Flight Id {1}), ", flight.Number, flight.Id);
                }
                if (flightsHosted != "")
                {
                    flightsHosted = flightsHosted.Substring(0, flightsHosted.Length - 1);
                }
            }
            else
            {
                flightsHosted = "No flights hosted yet";
            }
            return String.Format("Airport Id: {0}; Airport Code: {1}; Flights Hosted: {2}", Id, Code, flightsHosted);
        }
    }
}
