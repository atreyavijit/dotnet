﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    class SuperFib
    {
        private List<long> ComputedSuperFibs = new List<long>();
        public long this[int index]
        {
            get
            {
                if(ComputedSuperFibs.Count > index )
                {
                    return ComputedSuperFibs[index];
                }
                else
                {
                    if (index < 1)
                    {
                        throw new ArgumentException("Fibs only exist for positive numbers");
                    }
                    long f1 = 0, f2 = 1, f3 = 1;
                    long fib = 0;
                    for (int i = 1; i <= index; i++)
                    {
                        if(i == 1)
                        {
                            fib = 0;
                        }
                        else if (i == 2 || i == 3)
                        {
                            fib = 1;
                        }
                        else
                        {
                            fib = f1 + f2 + f3;
                            f1 = f2;
                            f2 = f3;
                            f3 = fib;
                        }
                        ComputedSuperFibs.Add(fib);
                    }
                    foreach(long f in ComputedSuperFibs)
                    {
                        Console.Write("{0} ", f);
                    }
                    return fib;
                }
            }
            set
            {

            }
        }
    }
}
