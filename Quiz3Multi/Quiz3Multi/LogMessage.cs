﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public class LogMessage
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Msg { get; set; }
    }
}
