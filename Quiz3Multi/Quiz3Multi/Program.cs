﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz3Multi
{
    public delegate void LogMessageDelegate(string msg);
    class Program
    {
        static LogMessageDelegate Logger;
        static AirTravelContext ctx;
        
        static void Main(string[] args)
        {
            try
            {
                ctx = new AirTravelContext();
                Logger += LogToScreen;
                Logger += LogToFile;
                Logger += LogToDatabase;
                int choice;
                do
                {
                    try
                    {
                        choice = ReadUserChoice();
                        switch (choice)
                        {
                            case 1:
                                AddAirport();
                                break;
                            case 2:
                                RegisterFlight();
                                break;
                            case 3:
                                ShowAllFlightsInvolvingASpecificAirport();
                                break;
                            case 4:
                                DeleteAirportAndAllAssociatedFlights();
                                break;
                            case 5:
                                DisplayNthSuperFibNumber();
                                break;
                            case 6:
                                ChangeExceptionHandlersDelegates();
                                break;
                            case 0:
                                Console.WriteLine("Good Bye");
                                break;
                            default:
                                Logger("Error: Invalid Choice\n");
                                break;
                        }
                        if (choice == 0)
                        {
                            break;
                        }
                    }
                    catch (InvalidDataException ex)
                    {
                        Logger("Exception: " + ex.Message);
                    }
                    catch (DataException ex)
                    {
                        Logger("Exception: " + ex.Message);
                    }
                    catch (IOException ex)
                    {
                        Logger("Exception: " + ex.Message);
                    }
                } while (true);
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }

        //to read user choice
        static int ReadUserChoice()
        {
            int choice;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("***SELECT YOUR CHOICE***");
                Console.WriteLine("1. Add airport");
                Console.WriteLine("2. Register a flight from/to airports");
                Console.WriteLine("3. Show all flights involving a specific airport(from or to) by airport code");
                Console.WriteLine("4. Delete an airport and all flights associated with it");
                Console.WriteLine("5. Display Nth SuperFib number");
                Console.WriteLine("6. Change exception handlers delegates");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Error: Invalid Choice\n");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return choice;
        }

        //to change exception handler delegate
        private static void ChangeExceptionHandlersDelegates()
        {
            string choiceStr;

            Console.WriteLine();
            Console.WriteLine("***SELECT YOUR LOGGER CHOICE***");
            Console.Write("Which loggers would you like to enable? Specify comma - separated list, empty for none: ");
            Console.Write("1.to console / screen \n");
            Console.Write("2.to file \n");
            Console.Write("3.to database \n");
            Console.Write("Choice: ");
            choiceStr = Console.ReadLine();
            if (choiceStr == "")
            {
                return;
            }

            //if (!Regex.Match(choiceStr, @"^([1-3]{1}[,]{1}){0-2}[1-3]{1}$").Success)
            //{
            //    Logger("Error: Invalid choice. Must be single digits 1 to 3, separated by commas and a maximum of 3 times");
              //  return;
            //}
            Logger = null;
            string[] choiceArray = choiceStr.Split(',');
            foreach (string choice in choiceArray)
            {
                switch (choice)
                {
                    case "1":
                        Logger += LogToScreen;
                        break;
                    case "2":
                        Logger += LogToFile;
                        break;
                    case "3":
                        Logger += LogToDatabase;
                        break;
                    default:
                        Logger += LogToScreen;
                        Logger += LogToFile;
                        Logger += LogToDatabase;
                        break;
                }
            }
        }

        private static void DisplayNthSuperFibNumber()
        {
            Console.Write("Which super fibonacci number you want to display: ");
            int index;
            if (!int.TryParse(Console.ReadLine(), out index))
            {
                Logger("Error:Index must be an integer");
                return;
            }
            SuperFib sf = new SuperFib();
            Console.WriteLine("The {0}th super fibonacci term is: {1}", index, sf[index]);
        }

        //delete airport with all associated flights
        private static void DeleteAirportAndAllAssociatedFlights()
        {
            ListAllAirports();
            Console.Write("Enter airport Id to delete: ");
            int airportId;
            if (!int.TryParse(Console.ReadLine(), out airportId))
            {
                Logger("Error: Invalid airport Id. Must be a integer");
                return;
            }

            var airportList = (from a in ctx.Airports where a.Id == airportId select a).ToList();

            Airport airportToDelete = airportList.Count == 0 ? null : airportList[0];

            if (airportToDelete == null)
            {
                Logger("Error: The airport Id you specified doesn't exist");
                return;
            }
            else
            {
                foreach (Flight currentFlight in airportToDelete.FlightsList.ToList())
                {
                    ctx.Flights.Remove(currentFlight);
                }
                ctx.Airports.Remove(airportToDelete);
                ctx.SaveChanges();
                Console.WriteLine("The airport with Id: {0}, along with all associated flights, has been deleted", airportId);
            }
        }


        //show all flights involving a specific airport
        private static void ShowAllFlightsInvolvingASpecificAirport()
        {
            Console.Write("Enter the airport code to search: ");
            string airportCodeToSearch = Console.ReadLine();
            if (!Regex.Match(airportCodeToSearch, "^[A-Z]{3}$").Success)
            {
                Logger("Error: Invalid Airport Code. Must be three letters all uppsercase.");
                return;
            }
            var searchFlightList = (from f in ctx.Flights where f.FromAirport.Code == airportCodeToSearch || f.ToAirport.Code == airportCodeToSearch select f).ToList();
            foreach (Flight flight in searchFlightList)
            {
                Console.WriteLine(flight);
            }
        }


        //register a flight
        private static void RegisterFlight()
        {
            Console.Write("Enter flight number (must be two letters followed by two or three digits): ");
            string flightNumber = Console.ReadLine();
            if (!Regex.Match(flightNumber, "^[A-Za-z]{2}[0-9]{2,3}$").Success)
            {
                Logger("Error: Invalid flight number. Must be two letters followed by two or three digits");
                return;
            }
            ListAllAirports();
            Console.Write("Enter origin airport Id for this flight: ");
            int fromAirportId;
            if (!int.TryParse(Console.ReadLine(), out fromAirportId))
            {
                Logger("Error: Invalid airport Id. Must be a integer");
                return;
            }
            Console.Write("Enter destination airport Id for this flight: ");
            int toAirportId;
            if (!int.TryParse(Console.ReadLine(), out toAirportId))
            {
                Logger("Error: Invalid airport Id. Must be a integer");
                return;
            }

            if (fromAirportId == toAirportId)
            {
                Logger("Error: Origin Airport Id and Destination Airport Id are same. You can't add a flight from one airport to itself");
                return;
            }

            var fromAirportList = (from a in ctx.Airports where a.Id == fromAirportId select a).ToList();
            Airport fromAirport = fromAirportList.Count == 0 ? null : fromAirportList[0];

            var toAirportList = (from a in ctx.Airports where a.Id == toAirportId select a).ToList();
            Airport toAirport = toAirportList.Count == 0 ? null : toAirportList[0];

            Flight currentFlight = new Flight() { Number = flightNumber, FromAirport = fromAirport, ToAirport = toAirport };
            Console.WriteLine("Flight number {0} has been registered and scheduled to fly from airport {1}({2}) to airport {3}({4})", currentFlight.Number, fromAirport.Code, fromAirport.City, toAirport.Code, toAirport.City);
            ctx.Flights.Add(currentFlight);
            ctx.SaveChanges();
        }


        //add airport
        private static void AddAirport()
        {
            Console.Write("Enter Airport Code (3 Letters, all uppercase): ");
            string airportCode = Console.ReadLine();

            if (!Regex.Match(airportCode, "^[A-Z]{3}$").Success)
            {
                Logger("Error: Invalid Airport Code. Must be three letters all uppsercase.");
                return;
            }
            Console.Write("Enter City of the Airport(Max 50 letters): ");
            string airportCity = Console.ReadLine();

            if (airportCity.Length < 0 || airportCity.Length > 50)
            {
                Logger("Error: Invalid City. Must be 0-50 characters.");
                return;
            }
            Airport currentAirport = new Airport() { Code = airportCode, City = airportCity };
            ctx.Airports.Add(currentAirport);
            ctx.SaveChanges();
            Console.WriteLine("Airport " + currentAirport + " added successfully");
        }

        //list airports
        static void ListAllAirports()
        {
            List<Airport> airports = (from a in ctx.Airports select a).ToList();
            if (airports.Count == 0)
            {
                Console.WriteLine("No airport exist");
                return;
            }
            Console.WriteLine("List of existing airports:");
            foreach (var a in airports)
            {
                Console.WriteLine(a);
            }
        }


        //log to screen
        public static void LogToScreen(string msg)
        {
            if (msg != null)
            {
                Console.WriteLine(msg);
            }
        }

        //log to file
        public static void LogToFile(string msg)
        {
            if (msg != null)
            {
                const string FILE_PATH = @"..\..\..\main.log";
                File.AppendAllText(FILE_PATH, string.Format(DateTime.Now.ToString("yyyy'-'MM'-'dd' 'HH':'mm':'ss") + " " + msg + Environment.NewLine));
            }
        }
        public static void LogToDatabase(string msg)
        {
            if (msg != null)
            {
                LogMessage logMessage = new LogMessage() { Timestamp = DateTime.Now, Msg = msg };
                ctx.LogMessages.Add(logMessage);
                ctx.SaveChanges();
            }
        }

    }
}
