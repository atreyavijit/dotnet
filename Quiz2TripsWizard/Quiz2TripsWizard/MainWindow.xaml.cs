﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<TripWithCities> TripList = new List<TripWithCities>();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                Globals.Db = new Database();
                // 2 - load data in ListView
                ReloadList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Todos Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadList()
        {
            try
            {
                List<TripWithCities> list = Globals.Db.GetAllTripWithCities();
                TripList.Clear();
                foreach (TripWithCities t in list)
                {
                    t.CalculateDistance();
                    TripList.Add(t);
                    
                }
                lvTrips.ItemsSource = TripList;
                //lvTrips.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Todos Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void DataExport_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveDialog = new SaveFileDialog();
            saveDialog.Filter = "Todo files|*.todo|All files|*.*";
            saveDialog.Title = "Export Data To File";
            saveDialog.ShowDialog();
            if (saveDialog.FileName != "")
            {
                if (lvTrips.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Select data to export");
                    return;
                }
                List<TripWithCities> selectedItemsList = new List<TripWithCities>();
                foreach (TripWithCities trip in lvTrips.SelectedItems)
                {
                    selectedItemsList.Add(trip);
                }
                Globals.extFile = new ExternalFile();
                Globals.extFile.WriteDataToFile(saveDialog.FileName, selectedItemsList);
            }
        }

        private void AddTrip_MenuClick(object sender, RoutedEventArgs e)
        {
            ChooseCities chooseCitiesDialog = new ChooseCities(this);
            chooseCitiesDialog.ShowDialog();
            ReloadList();
        }
    }
}
