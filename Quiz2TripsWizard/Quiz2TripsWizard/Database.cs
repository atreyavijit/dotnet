﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Vijit\Documents\dotnet\Quiz2TripsWizard\TripWizardDB.mdf;Integrated Security=True;Connect Timeout=30";
        private SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        //select cities from table Cities
        public List<City> GetAllCities()
        {
            List<City> list = new List<City>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Cities", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string cityName = (string)reader[1];
                    Double latitude = (double)reader[2];
                    Double longitude = (double)reader[3];
                    list.Add(new City() { Id = id, CityName = cityName, Latitude =latitude, Longitude = longitude});
                }
            }
            return list;
        }

        //insert data into table Trips
        public void AddTrip(Trip trip)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Trips(PersonName,DepartureDate,ReturnDate,FromCityId,ToCityID) VALUES (@name,@departure,@return,@from,@to)", conn);
            cmdInsert.Parameters.AddWithValue("name", trip.PersonName);
            cmdInsert.Parameters.AddWithValue("departure", trip.DepartureDate);
            cmdInsert.Parameters.AddWithValue("return", trip.ReturnDate);
            cmdInsert.Parameters.AddWithValue("from", trip.FromCityId);
            cmdInsert.Parameters.AddWithValue("to", trip.ToCityId);
            cmdInsert.ExecuteNonQuery();
        }

        //fetch trip with cities information
        public List<TripWithCities> GetAllTripWithCities()
        {
            List<TripWithCities> list = new List<TripWithCities>();
            SqlCommand cmdSelect = new SqlCommand("SELECT t.Id, t.personName, cFrom.CityName, cTo.CityName, t.Departuredate, t.ReturnDate, cFrom.Latitude, cFrom.Longitude, cTo.Latitude, cTo.Longitude, cFrom.Id, cTo.Id FROM trips AS t INNER JOIN Cities cFrom ON t.FromCityId = cFrom.Id INNER JOIN Cities AS cTo ON t.ToCityId = cTo.Id", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string personName = (string)reader[1];
                    string fromCity = (string)reader[2];
                    string toCity = (string)reader[3];
                    DateTime departureDate = (DateTime)reader[4];
                    DateTime returnDate = (DateTime)reader[5];
                    double latFrom = (double)reader[6];
                    double lonFrom = (double)reader[7];
                    double latTo = (double)reader[8];
                    double lonTo = (double)reader[9];
                    int fromCityId = (int)reader[10];
                    int toCityId = (int)reader[11];
                    list.Add(new TripWithCities()
                    {
                        TripId = id,
                        PersonName = personName,
                        FromCityId = fromCityId,
                        ToCityId = toCityId,
                        FromCityName = fromCity,
                        ToCityName = toCity,
                        DepartureDate = departureDate,
                        ReturnDate = returnDate,
                        FromCityLatitude = latFrom,
                        FromCityLongitude = lonFrom,
                        ToCityLatitude = latTo,
                        ToCityLongitude = lonTo
                    });
                    
                }
            }
            return list;
        }
    }
}