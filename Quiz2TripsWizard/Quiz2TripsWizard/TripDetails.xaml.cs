﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for TripDetails.xaml
    /// </summary>
    public partial class TripDetails : Window
    {
        public TripDetails(Window owner)
        {
            InitializeComponent();
            owner.Hide();
            Owner = owner;
            lbFromCity.Content = Globals.FromCity.CityName;
            lbToCity.Content = Globals.ToCity.CityName;
        }

        private void BtFinish_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            if (tbName.Text.Length < 1 || tbName.Text.Length > 100)
            {
                message += "Name must be between 1 to 100 character" + Environment.NewLine;
            }
            if(dpDepartureDate.SelectedDate.ToString()=="")
            {
                message += "Please select a departure date" + Environment.NewLine;
            }
            if (dpReturnDate.SelectedDate.ToString() == "")
            {
                message += "Please select a departure date" + Environment.NewLine;
            }
            if(dpDepartureDate.SelectedDate > dpReturnDate.SelectedDate)
            {
                message += "Return date must be greater than departure date" + Environment.NewLine;
            }
            if(message != "")
            {
                MessageBox.Show(message);
                return;
            }
            Trip trip = new Trip() { PersonName = tbName.Text, DepartureDate = (DateTime)dpDepartureDate.SelectedDate, ReturnDate = (DateTime)dpReturnDate.SelectedDate,FromCityId = Globals.FromCity.Id, ToCityId=Globals.ToCity.Id};
            try
            {
                Globals.Db.AddTrip(trip);
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Error Connecting to Database" + Environment.NewLine + ex.Message);
            }
            this.Hide();
        }
    }
}
