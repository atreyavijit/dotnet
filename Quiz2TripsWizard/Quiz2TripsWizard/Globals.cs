﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    class Globals
    {
        public static City FromCity;
        public static City ToCity;
        public static Database Db;
        public static ExternalFile extFile;
    }
}
