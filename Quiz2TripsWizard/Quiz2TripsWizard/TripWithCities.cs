﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz2TripsWizard
{
    public class TripWithCities
    {
        public int TripId { get; set; }
        public string PersonName { get; set; }
        public int FromCityId { get; set; }
        public int ToCityId { get; set; }
        public string FromCityName { get; set; }
        public string ToCityName { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ReturnDate { get; set; }
        public double FromCityLatitude { get; set; }
        public double FromCityLongitude { get; set; }
        public double ToCityLatitude { get; set; }
        public double ToCityLongitude { get; set; }
        public double TripDistanceKm { get; set; }
        public void CalculateDistance()
        {
            double dDistance = Double.MinValue;
            double dFromCityLatitudeInRad = FromCityLatitude * (Math.PI / 180.0);
            double dFromCityLongitudeInRad = FromCityLongitude * (Math.PI / 180.0);
            double dToCityLatitudeInRad = ToCityLatitude * (Math.PI / 180.0);
            double dToCityLongitudeInRad = ToCityLongitude * (Math.PI / 180.0);

            double dLongitude = dToCityLongitudeInRad - dFromCityLongitudeInRad;
            double dLatitude = dToCityLatitudeInRad - dFromCityLatitudeInRad;

            // Intermediate result a.
            double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                       Math.Cos(dFromCityLatitudeInRad) * Math.Cos(dToCityLatitudeInRad) *
                       Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            double c = 2.0 * Math.Asin(Math.Sqrt(a));

            // Distance.
            // const Double kEarthRadiusMiles = 3956.0;
            const Double kEarthRadiusKms = 6376.5;
            dDistance = kEarthRadiusKms * c;
            TripDistanceKm = dDistance;
        }
    }
}
