﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Quiz2TripsWizard
{
    class ExternalFile
    {
        public void WriteDataToFile(string fileName, List<TripWithCities> tripList)
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] tripLines = new String[tripList.Count];
                int i = 0;
                foreach (TripWithCities trip in tripList)
                {
                    tripLines[i++] = String.Format("{0};{1};{2};{3};{4};{5};{6}", trip.TripId,trip.PersonName,trip.FromCityName,trip.ToCityName,trip.TripDistanceKm,trip.DepartureDate.ToShortDateString(),trip.ReturnDate.ToShortDateString());
                }
                File.WriteAllLines(fileName, tripLines);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: failed to save data to file: {0}", ex.Message);
            }
        }
    }
}
