﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Quiz2TripsWizard
{
    /// <summary>
    /// Interaction logic for ChooseCities.xaml
    /// </summary>
    public partial class ChooseCities : Window
    {
        static List<City> cityList = new List<City>();

        public ChooseCities(Window owner)
        {
            InitializeComponent();
            try
            {
                cityList = Globals.Db.GetAllCities();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error Connecting to Database" + Environment.NewLine + ex.Message);
            }
            lvFromCities.ItemsSource = cityList;
            lvToCities.ItemsSource = cityList;
            Owner = owner;
        }

        private void BtNext_Click(object sender, RoutedEventArgs e)
        {
            if(lvFromCities.SelectedItems.Count != 1 && lvFromCities.SelectedItems.Count != 1)
            {
                MessageBox.Show("Please selected exactly one city from each of the lists");
                return;
            }
            if (lvFromCities.SelectedItem == lvToCities.SelectedItem)
            {
                MessageBox.Show("From city and to city must be different");
                return;
            }
            Globals.FromCity = lvFromCities.SelectedItem as City;
            Globals.ToCity = lvToCities.SelectedItem as City;
            TripDetails tripDetailsDialog = new TripDetails(this);
            tripDetailsDialog.ShowDialog();
        }
    }
}
