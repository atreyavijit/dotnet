﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day01review
{
    class Program
    {
        static void Main(string[] args)
        {
            string minString, maxString, countString;
            int minimum, maximum, count;
            Random rand = new Random();

            Console.Write("Please enter your minimum integer: ");
            minString = Console.ReadLine();
            while (!int.TryParse(minString, out minimum))
            {
                Console.Write("Invalid Integer. Please enter a valid integer: ");
                minString = Console.ReadLine();
            }

            Console.Write("Please enter your maximum integer: ");
            maxString = Console.ReadLine();
            while (!int.TryParse(maxString, out maximum))
            {
                Console.Write("Invalid Integer. Please enter a valid integer: ");
                maxString = Console.ReadLine();
            }

            Console.Write("How many numbers do you want: ");
            countString = Console.ReadLine();
            while (!int.TryParse(countString, out count))
            {
                Console.Write("Invalid Integer. Please enter a valid integer: ");
                countString = Console.ReadLine();
            }
            if (minimum >= maximum)
            {
                Console.WriteLine("Error: minimum can not be larger than maximum");
                //Environment.Exit(1);
                return;
            }
            if (count<1)
            {
                Console.WriteLine("Error: you must generate 1 or more numbers");
                return;
            }
            Console.Write("Numbers Generated:");
            for(int i=1; i<=count;i++)
            {
                Console.Write(i==1?"{0}": ", {0}", rand.Next(minimum, maximum+1));   // Minimum and Maximum both inclusive
                   
            }
            Console.Write("\nPress any key to end");
            Console.ReadKey();
        }
    }
}
