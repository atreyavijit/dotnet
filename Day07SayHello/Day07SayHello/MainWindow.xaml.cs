﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07SayHello
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public bool CheckData(string name, string ageStr)
        {
            int age;
            string message = "";
            if (!Regex.Match(name,@"^[^;]{2,30}$").Success)
            {
                message += "Name field must be between 2 to 30 characters, no semicolons\n";
            }
            if(ageStr == "")
            {
                message += "Age field is empty\n";
            }
            if(!int.TryParse(ageStr, out age) ||age < 1 || age >150)
            {
                message += "Age must be an integer between 1 to 150\n";
            }
            if(message == "")
            {
                return true;
            }
            MessageBox.Show(message);
            return false;
        }
        private void Greetuser(object sender, RoutedEventArgs e)
        {
            if(CheckData(tbName.Text,tbAge.Text) == true)
            {
                MessageBox.Show(string.Format("Hello {0}! you are {1} y/o", tbName.Text, int.Parse(tbAge.Text)));
            }
            return;
        }

        private void SaveData(object sender, RoutedEventArgs e)
        {

            const string FILEPATH = @"..\..\..\people.txt";
            if (CheckData(tbName.Text, tbAge.Text) == true)
            {
                File.AppendAllText(FILEPATH, string.Format("{0};{1}{2}", tbName.Text, int.Parse(tbAge.Text), Environment.NewLine));
                tbName.Text = "";
                tbAge.Text = "";
            }
            return;
        }
    }
}
