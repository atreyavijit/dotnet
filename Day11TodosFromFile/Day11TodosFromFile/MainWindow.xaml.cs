﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day11TodosFromFile
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class TodoItem
    {
        public string Task;
        public DateTime DueDate;
        public bool IsDone;
        public override string ToString()
        {
            string status = IsDone == true ? "Done" : "Pending";
            //string dateString = string.Format("{0}/{1}/{2}", DueDate.Month, DueDate.Day, DueDate.Year);
            return string.Format("{0} by {1} is {2}", Task, DueDate.ToShortDateString(), status);
        }
    }
    public partial class MainWindow : Window
    {
        static List <TodoItem> todoList = new List<TodoItem>();
        const string FILEPATH = @"..\..\..\todolist.dat";
        bool hasUnsavedData = false;
        public MainWindow()
        {
            InitializeComponent();
            LoadFromFile(FILEPATH);
            lvTodoList.ItemsSource = todoList;
        }

        private void LoadFromFile(string file)
        {
            try
            {
                string[] todoLines = File.ReadAllLines(file);
                string message = "";
                int todoCount = 0;
                foreach (string todo in todoLines)
                {
                    string[] data = todo.Split(';');
                    string task;
                    if (data.Length != 3)
                    {
                        message += string.Format("Corrupted line. Must have exactly three data: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (!Regex.Match(data[0], @"^[^;]{1,100}$").Success)
                    {
                        message += string.Format("Corrupted line. Task must be 1 to 100 characters, without semicolon : {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (!Regex.Match(data[1], @"^[0-9]{4}[-][0-9]{1,2}[-][0-9]{1,2}$").Success)
                    {
                        message += string.Format("Corrupted line. Date Format Invalid: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    if (data[2] != "Pending" && data[2] != "Done")
                    {
                        message += string.Format("Corrupted line. Status must be either Pending or Done: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    task = data[0];
                    //string[] date = data[1].Split('-');
                    //DateTime dueDate = new DateTime(int.Parse(date[1]),int.Parse(date[2]),int.Parse(date[0]));
                    if(!DateTime.TryParse(data[1], out DateTime dueDate))
                    {
                        message += string.Format("Corrupted line. Date Format Invalid: {0}{1}", todo, Environment.NewLine);
                        continue;
                    }
                    //isDone = data[2] == "Pending" ? false : true;
                    bool isDone;
                    if(data[2] == "Pending")
                    {
                        isDone = false;
                    }
                    else
                    {
                        isDone = true;
                    }
                    try
                    {
                        TodoItem td = new TodoItem()
                        {
                            Task = task,
                            DueDate = dueDate,
                            IsDone = isDone
                        };
                        todoList.Add(td);
                        todoCount++;
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        message += string.Format("Corrupted line: {0}. {1}{2}", todo, ex.Message, Environment.NewLine);
                    }
                }
                sbLastOp.Text = string.Format("{0} data have been loaded from file: {1}", todoCount, file);
                if (message != "")
                {
                    MessageBox.Show(message);
                }
            }
            catch (FileNotFoundException)
            {
                //do nothing
                //its okay to start this program without the file
                //you can read data which will ultimately be written to file at the end
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: Can't read from the file: {0}", ex.Message);
            }
        }
        private void SaveToFile(string file, IList<TodoItem> itemsList)
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] todoLines = new String[itemsList.Count];
                int i = 0;
                foreach (TodoItem td in itemsList)
                {
                    string date = String.Format("{0}-{1}-{2}", td.DueDate.Year, td.DueDate.Month, td.DueDate.Day);
                    string status = td.IsDone == true ? "Done" : "Pending";
                    todoLines[i++] = String.Format("{0};{1};{2}", td.Task,date,status);
                }
                File.WriteAllLines(file, todoLines);
                sbLastOp.Text = string.Format("{0} data have been written to file: {1}", itemsList.Count, file);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error: failed to save data to file: {0}", ex.Message);
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveToFile(FILEPATH,todoList);
            Close();
        }

        private void FileImport_MenuClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "Todo files|*.todos";
            dialog.Title = "Save data to a file";
            dialog.ShowDialog();
            if (dialog.FileName != "")
            {
                LoadFromFile(dialog.FileName);
            }
        }

        private void FileExport_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Todo files|*.todos";
            dialog.Title = "Save data to a file";
            dialog.ShowDialog();
            if (dialog.FileName != "")
            {
                if(lvTodoList.SelectedItems.Count == 0)
                {
                    MessageBox.Show("Select data to export");
                    return;
                }
                List<TodoItem> selectedItemsList = new List<TodoItem>();
                foreach(TodoItem item in lvTodoList.SelectedItems)
                {
                    selectedItemsList.Add(item);
                }
                SaveToFile(dialog.FileName, selectedItemsList);
                hasUnsavedData = false;
            }
        }

        private void SortByTask_MenuClick(object sender, RoutedEventArgs e)
        {
            /*
            TodoItem todoTemp;
            int i, j;
            for(i = 0; i < todoList.Count - 1; i++)
            {
                for(j = i + 1; j < todoList.Count; j++)
                {
                    if(string.Compare(todoList[i].Task,todoList[j].Task) == 1)
                    {
                        todoTemp = todoList[i];
                        todoList[i] = todoList[j];
                        todoList[j] = todoTemp;
                    }
                }
            }
            lvTodoList.Items.Refresh();
            */
            todoList = todoList.OrderBy(todoItem => todoItem.Task).ToList<TodoItem>();
            lvTodoList.ItemsSource = todoList;
        }

        private void SortByDueDate_MenuClick(object sender, RoutedEventArgs e)
        {
            TodoItem todoTemp;
            int i, j;
            for (i = 0; i < todoList.Count - 1; i++)
            {
                for (j = i + 1; j < todoList.Count; j++)
                {
                    if (todoList[i].DueDate > todoList[j].DueDate)
                    {
                        todoTemp = todoList[i];
                        todoList[i] = todoList[j];
                        todoList[j] = todoTemp;
                    }
                }
            }
            lvTodoList.Items.Refresh();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hasUnsavedData == true)
            {
                MessageBoxResult result = MessageBox.Show("You have data that are not exported. Are you sure you want to quit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (result == MessageBoxResult.No)
                {
                    e.Cancel = true;
                }
            }
            SaveToFile(FILEPATH,todoList);
        }

        private void AddUpdateTask_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            if (!Regex.Match(tbTask.Text, @"^[^;]{1,100}$").Success)
            {
                message += "Task must be 1 to 100 character long, without semicolon"+Environment.NewLine;
            }
            if (!DateTime.TryParse(dpDueDate.Text, out DateTime dueDate) || !Regex.Match(dpDueDate.Text, @"^[0-9]{1,2}[/][0-9]{1,2}[/][0-9]{4}$").Success)
            {
                message += "Date Format Invalid"+Environment.NewLine;
            }
            if(rbTaskDone.IsChecked == false && rbTaskPending.IsChecked == false)
            {
                message += "Select a status"+Environment.NewLine;
            }
            if(message != "")
            {
                MessageBox.Show(message);
                return;
            }
            string task = tbTask.Text;
            bool isDone;
            if(rbTaskDone.IsChecked == true)
            {
                isDone = true;
            }
            else if(rbTaskPending.IsChecked == true)
            {
                isDone = false;
            }
            else
            {
                MessageBox.Show("Internal Error");
                return;
            }
            Button senderButton = sender as Button;
            if (senderButton.Name == "btUpdateTask")
            {
                TodoItem todo = lvTodoList.SelectedItem as TodoItem;
                todo.Task = task;
                todo.DueDate = dueDate;
                todo.IsDone = isDone;
            }
            else
            {
                TodoItem todo = new TodoItem()
                {
                    Task = task,
                    DueDate = dueDate,
                    IsDone = isDone
                };
                todoList.Add(todo);
                hasUnsavedData = true;
                tbTask.Text = "";
                dpDueDate.Text = "";
                rbTaskDone.IsChecked = false;
                rbTaskPending.IsChecked = true;
            }
            lvTodoList.Items.Refresh();
        }
        private void DeleteTask_Click(object sender, RoutedEventArgs e)
        {
            TodoItem item = lvTodoList.SelectedItem as TodoItem;
            MessageBoxResult result = MessageBox.Show("Are you sure you want to delete:\n"+item.ToString(), "Todo List", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if(result == MessageBoxResult.OK)
            {
                todoList.Remove(item);
                lvTodoList.Items.Refresh();
            }
        }

        private void LvTodoList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lvTodoList.SelectedItems.Count != 1)
            {
                btUpdateTask.IsEnabled = false;
                if (lvTodoList.SelectedItems.Count == 0)
                {
                    btDeleteTask.IsEnabled = false;
                }
                tbTask.Text = "";
                dpDueDate.SelectedDate = null;
                rbTaskPending.IsChecked = true;
                return;
            }
            TodoItem item = lvTodoList.SelectedItem as TodoItem;
            btUpdateTask.IsEnabled = true;
            btDeleteTask.IsEnabled = true;
            tbTask.Text = item.Task;
            dpDueDate.SelectedDate = item.DueDate;
            rbTaskDone.IsChecked = item.IsDone;
        }
    }
}
