﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Names
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> nameList = new List<string>();
            string currentName;
            try
            {
                Console.WriteLine("This program will continuously asks you to provide names. Press ENTER key to terminate: ");
                do
                {
                    Console.Write("Enter a name: ");
                    currentName = Console.ReadLine();
                    if (currentName == "")
                        break;
                    nameList.Add(currentName);

                } while (true);
                Console.WriteLine("\nYou have provided an empty name.");
                Console.WriteLine("\nThe names that you entered are: ");
                foreach (string name in nameList)
                {
                    Console.WriteLine("{0}", name);
                }
            }
            finally
            {
                Console.Write("Press any key to continue ...");
                Console.ReadKey();
            }
        }
    }
}
