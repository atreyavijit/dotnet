﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02ArrayContains
{
    class Program
    {
        // Concatenate arrays
        public static int[] Concatenate(int[] a1, int[] a2)
        {
            int[] c = new int[a1.Length+a2.Length];
            int i, j;
            for (i = 0; i < a1.Length; i++)
            {
                c[i] = a1[i];
            }
            for (j = 0; j<a2.Length; j++)
            {
                c[i + j] = a2[j];
            }
            return c;
        }
        //print duplicates
        public static void PrintDups(int[] a1, int[] a2)
        {
            for (int i=0;i<a1.Length;i++)
            {
                for(int j=0;j<a2.Length;j++)
                {
                    if(a1[i] == a2[j])
                    {
                        Console.WriteLine("{0}", a1[i]);
                        break;
                    }
                }
            }
        }
        //remove duplicates
        public static int[] RemoveDups(int[] a1, int[] a2)
        {
            int dupsCount1 = 0;
            int dupsCount2 = 0;
            int i,j,k = 0;
            int[] noDupsInitial = new int[a1.Length + a2.Length];
            for (i = 0; i < a1.Length; i++)
            {
                for (j = 0; j < a2.Length; j++)
                {
                    if (a1[i] == a2[j])
                    {
                        dupsCount1++;
                        break;
                    }
                }
                if(j == a2.Length)
                {
                    noDupsInitial[k++] = a1[i];
                }
            }
            for (i = 0; i < a2.Length; i++)
            {
                for (j = 0; j < a1.Length; j++)
                {
                    if (a2[i] == a1[j])
                    {
                        dupsCount2++;
                        break;
                    }
                }
                if (j == a1.Length)
                {
                    noDupsInitial[k++] = a2[i];
                }
            }
            int finalSize = a1.Length + a2.Length - dupsCount1 - dupsCount2;
            int[] noDups = new int[finalSize];
            k = 0;
            for (i = 0; i < noDupsInitial.Length; i++)
            {
                if (noDupsInitial[i] != 0)
                {
                    noDups[k++] = noDupsInitial[i];
                }
            }
            Console.WriteLine("\n\n\n{0} repeated data in first array\n{1} repeated data in second array\n\n\n", dupsCount1,dupsCount2);
            return noDups;
        }
        public static void PrintDups(int[,] a1, int [,]a2)
        {
            for(int i=0;i<a1.GetLength(0);i++)
            {
                for(int j=0;j<a1.GetLength(1);j++)
                {
                    for(int k=0;k<a2.GetLength(0);k++)
                    {
                        for(int l=0;l<a2.GetLength(1);l++)
                        {
                            if(a1[i,j] == a2[k,l])
                            {
                                Console.WriteLine("{0}", a1[i, j]);
                            }
                        }
                    }
                }
            }
            //Console.WriteLine("{0} {1}", a1.GetLength(0), a1.GetLength(1));
        }
        static void Main(string[] args)
        {
            int []c = Concatenate(new int[]{1,2},new int[]{2,3});
            Console.WriteLine("Concatenated Array is: ");
            for(int i=0;i<c.Length;i++)
            {
                Console.WriteLine("{0}", c[i]);
            }
            Console.WriteLine("\n\nThe duplicate values are: ");
            PrintDups(new int[] { 1, 3, 7, 8, 2, 7, 9, 11 }, new int[] { 3, 8, 7, 5, 13, 5, 12 });
            c = RemoveDups(new int[] { 2, 3, 5, 7, 3, 2 }, new int[] { 3, 9, 7 });
            for (int i = 0; i < c.Length; i++)
            {
                Console.WriteLine("{0}", c[i]);
            }
            Console.WriteLine("\n\n\nThe duplicate items are:");
            PrintDups(new int[,] { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10, 11 } }, new int[,] { { 0, 11, 22, 3 }, { 41, 52, 63, 7 }, { 82, 9, 10, 11 } });
            Console.ReadKey();
        }
    }
}
