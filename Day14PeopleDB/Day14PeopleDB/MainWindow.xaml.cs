﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day14PeopleDB
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Person> PeopleList = new List<Person>();
        public MainWindow()
        {
            InitializeComponent();
            lvPeople.ItemsSource = PeopleList;
            try
            {
                // 1 - Connect to database
                Globals.Db = new Database();
                // 2 - Load data in ListView
                ReloadList();
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Fatal Error. Unable to connect database:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // closes the main window, terminates the program
            }
        }

        private void ReloadList()
        {
            try
            {
                List<Person> list = Globals.Db.GetAllPeople();
                PeopleList.Clear();
                foreach (Person p in list)
                {
                    PeopleList.Add(p);
                }
                lvPeople.Items.Refresh();
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Error. Executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddPerson_MenuClick(object sender, RoutedEventArgs e)
        {
            AddEditDialog addEditDialog = new AddEditDialog(this);
            if(addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {

        }
        private void DeleteItem_ContextMenuClick(object sender, RoutedEventArgs e)
        {
            Person person = lvPeople.SelectedItem as Person;
            if(lvPeople.Items.Count == 0)
            {
                MessageBox.Show("There are no records to delete");
            }
            if(person == null)
            {
                MessageBox.Show("Please select a person to delete");
                return;
            }
            MessageBoxResult result = MessageBox.Show(this, "Are you sure your want to delete:\n" + person, "Confirm Delete", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    Globals.Db.DeletePerson(person.Id);
                    ReloadList();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Error. Executing SQL query:\n" + ex.Message, "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void EditItem_ContextMenuClick(object sender, RoutedEventArgs e)
        {
            Person person = lvPeople.SelectedItem as Person;
            if (lvPeople.Items.Count == 0)
            {
                MessageBox.Show("There are no records to edit");
            }
            if (person == null)
            {
                MessageBox.Show("Please select a person to edit");
                return;
            }
            AddEditDialog addEditDialog = new AddEditDialog(this, person);
            if (addEditDialog.ShowDialog() == true)
            {
                ReloadList();
            }
        }

        private void LvPeople_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            EditItem_ContextMenuClick(sender,null);
        }

        private void LvPeople_SortByHeader(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader selectedHeader = e.OriginalSource as GridViewColumnHeader; ;
            if (selectedHeader != null && selectedHeader.Content != null)
            {
                string sortParameter = selectedHeader.Content.ToString();
                //FIXME: ArgumentNullException, ArgumentException
                //PeopleList.Sort((person1,person2) => person1.Name.CompareTo(person2.Name));
            
                if (sortParameter == "Id")
                {
                    PeopleList = PeopleList.OrderBy(person => person.Id).ToList<Person>();
                }
                else if (sortParameter == "Name")
                {
                    PeopleList = PeopleList.OrderBy(person => person.Name).ToList<Person>();
                }
                else if (sortParameter == "Age")
                {
                    PeopleList = PeopleList.OrderBy(person => person.Age).ToList<Person>();
                }
                lvPeople.ItemsSource = PeopleList;
            }
            else return;
        }
    }
}
