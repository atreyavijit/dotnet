﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day14PeopleDB
{
    public class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\1896170\Documents\FirstDB.mdf;Integrated Security=True;Connect Timeout=30";

        private SqlConnection conn;
        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }
        public List<Person> GetAllPeople()
        {
            List<Person> peopleFromDatabase = new List<Person>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM People", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[0];
                    string name = (string)reader[1];
                    int age = (int)reader[2];
                    peopleFromDatabase.Add(new Person() { Id = id, Name = name, Age = age });
                }
            }
            return peopleFromDatabase;
        }
        public int AddPerson(Person person)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO People(Name, Age) OUTPUT INSERTED.ID VALUES (@Name, @Age)", conn);
            cmdInsert.Parameters.AddWithValue("Name", person.Name);
            cmdInsert.Parameters.AddWithValue("Age", person.Age);
            //cmdInsert.ExecuteNonQuery();
            int insertId = (int)cmdInsert.ExecuteScalar();
            person.Id = insertId;
            return insertId;
        }
        public bool UpdatePerson(Person person)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE People SET Name = @Name, Age = @age WHERE Id= @Id", conn);
            cmdUpdate.Parameters.AddWithValue("Id", person.Id);
            cmdUpdate.Parameters.AddWithValue("Name", person.Name);
            cmdUpdate.Parameters.AddWithValue("Age", person.Age);
            int rowsAffected = cmdUpdate.ExecuteNonQuery();
            return rowsAffected > 0; //if affected rows are greater than 0 it will return true otherwise it will return false
        }
        public bool DeletePerson(int id)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM People WHERE Id= @Id", conn);
            cmdDelete.Parameters.AddWithValue("Id", id);
            int rowsAffected = cmdDelete.ExecuteNonQuery();
            return rowsAffected > 0; //if affected rows are greater than 0 it will return true otherwise it will return false
        }
    }
}
