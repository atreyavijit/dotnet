﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day19FirstEF
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (var ctx = new FirstContext())
                {
                    //Equivalent of INSERT
                    Person p1 = new Person() { Name = "Bill" , Age = 30 };
                    ctx.People.Add(p1);
                    ctx.SaveChanges();
                }


            }
            finally
            {
                Console.Write("Press any key to exit");
                Console.ReadKey();
            }
        }
    }
}
