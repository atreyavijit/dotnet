﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10CarList
{
    public class Car
    {
        public string Make;
        public string CarSize;
        public string Features;
        public string Plates;
        public double WeightTonnes;
        public override string ToString()
        {
            return String.Format("{0} is {1} with {2} reg {3}, {4}T", Make, CarSize, Features, Plates, WeightTonnes);
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Car> carList = new List<Car>();
        bool hasUnsavedData = false;
        public MainWindow()
        {
            InitializeComponent();
            carList.Add(new Car()
            {
                Make = "Audi",
                CarSize = "Compact",
                Features = "",
                Plates = "A1B2C3",
                WeightTonnes = 1.54
            });
            carList.Add(new Car()
            {
                Make = "BMW",
                CarSize = "SUV",
                Features = "",
                Plates = "D4E5F6",
                WeightTonnes = 1.73
            });
            carList.Add(new Car()
            {
                Make = "Mercedes",
                CarSize = "Van",
                Features = "",
                Plates = "a4b5c6",
                WeightTonnes = 1.77
            });
            lvCars.ItemsSource = carList;
        }
        
        private void FileExportSelect_MenuClick(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "Text files|*.txt|All files|*.*";
            saveFileDialog1.Title = "Save data to a file";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != "")
            {
                List<string> linesList = new List<string>();
                foreach(Car car in lvCars.SelectedItems)
                {
                    linesList.Add(String.Format("{0};{1};{2};{3};{4}",car.Make,car.CarSize,car.Features,car.Plates,car.WeightTonnes));
                }
                try
                {
                    File.AppendAllLines(saveFileDialog1.FileName, linesList);
                    hasUnsavedData = false;
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error: failed to save data to file" + ex.Message);
                }
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ButtonAddAndClean_Click(object sender, RoutedEventArgs e)
        {
            string message = "";
            if(comboCarMake.Text == "")
            {
                message += "Select a Car Make" + Environment.NewLine;
            }
            if (!Regex.Match(tbPlates.Text, @"^[A-Z0-9]{3,9}$").Success)
            {
                message += "Plate must be 3-10 characters and only digits and uppercase are allowed" + Environment.NewLine;
            }
            if(message != "")
            {
                MessageBox.Show(message);
                return;
            }
            string make;
            string size = "";
            string features = "";
            string plates;
            double weightTonnes;
            make = comboCarMake.Text;
            if (rbCarSizeCompact.IsChecked == true)
            {
                size = "Compact";
            }
            else if (rbCarSizeVan.IsChecked == true)
            {
                size = "Van";
            }
            else if (rbCarSizeSUV.IsChecked == true)
            {
                size = "SUV";
            }
            else
            {
                MessageBox.Show("Internal Error");
                return;
            }
            if(cbFeaturesABS.IsChecked == true)
            {
                features += (string)cbFeaturesABS.Content + ","; 
            }
            if (cbFeaturesBluetooth.IsChecked == true)
            {
                features += (string)cbFeaturesBluetooth.Content + ",";
            }
            if (cbFeaturesOther.IsChecked == true)
            {
                features += (string)cbFeaturesOther.Content + ",";
            }
            if(features != "")
            {
                features = features.Substring(0, features.Length - 1);
            }
            plates = tbPlates.Text;
            weightTonnes = slWeight.Value;
            weightTonnes = Math.Round(weightTonnes, 2, MidpointRounding.AwayFromZero);
            Car car = new Car()
            {
                Make = make,
                CarSize = size,
                Features = features,
                Plates = plates,
                WeightTonnes = weightTonnes
            };
            carList.Add(car);
            hasUnsavedData = true;
            lvCars.Items.Refresh();
            comboCarMake.Text = "";
            rbCarSizeCompact.IsChecked = true;
            cbFeaturesABS.IsChecked = false;
            cbFeaturesBluetooth.IsChecked = false;
            cbFeaturesOther.IsChecked = false;
            tbPlates.Text = "";
            slWeight.Value = 2.50;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (hasUnsavedData == true)
            {
                MessageBoxResult result = MessageBox.Show("You have unsaved data. Are you sure you want to exit?", "Confirmation", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
                if (result == MessageBoxResult.Cancel)
                {
                    e.Cancel = true;
                }
            }
        }
    }
}
