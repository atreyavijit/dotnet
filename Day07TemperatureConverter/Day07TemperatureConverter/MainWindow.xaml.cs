﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TemperatureConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ButtonConvert_Click(object sender, RoutedEventArgs e)
        {
            double centigrade;
            if (!double.TryParse(tbCelsius.Text, out centigrade))
            {
                lblResult.Content = "Invalid Data";
            }
            else
            {
                double farenheit = 9/5 * centigrade + 32;
                lblResult.Content = String.Format("{0:0.##} farenheit",farenheit);
            }
        }
    }
}
