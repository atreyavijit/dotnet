﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07AllInputs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //private CheckData(string name, string ageOptionFirst, string ageOptionSecond, string ageOptionThird, string petCat, string petDog, string petOthers)

        private void ButtonRegisterMe_Click(object sender, RoutedEventArgs e)
        {
            string errorMessage = "";
            string name = tbName.Text;
            if (!Regex.Match(name, @"^[^;]{1,}$").Success)
            {
                errorMessage += "Name field can't be empty and can't contain semicolon\n";
            }
            string age;
            if(rbBelow18.IsChecked == true)
            {
                age = "Below 18";
            }
            else if (rb18to35.IsChecked == true)
            {

            }

            //CheckData(tbName.Text, rbBelow18.Content, rb18to35.Content, rb36AndUp.Content, cbCat.Content, cbDog.Content, cbOther.Content, cmbContinents.Text,slPrefTemp.);
        }

        
    }
}
