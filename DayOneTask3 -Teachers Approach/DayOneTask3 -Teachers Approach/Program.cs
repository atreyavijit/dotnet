﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        const int length = 5;
        static List<double> numbers = new List<double>();
        static double sum, avg;
        static void EnterValues()
        {
            for (int i = 0; i < length; i++)
            {
                Console.Write("Enter data number {0}: ",i+1);
                string line = Console.ReadLine();
                double value;
                if (!double.TryParse(line, out value))
                {
                    Console.WriteLine("Invalid input. Exiting.");
                    Environment.Exit(1);
                }
                numbers.Add(value);
            }
            Console.WriteLine();
        }

        static void FindSmallestnumber()
        {
            //double smallest = Int32.MaxValue;
            double smallest = numbers[0];
            foreach(double num in numbers)
            {
                smallest = num < smallest ? num : smallest;
            }
            Console.WriteLine("Smallest number is: {0}", smallest);
        }

        static void FindSumOfAllNumbers()
        {
            sum = 0.0;
            foreach (double num in numbers)
            {
                sum += num;
            }
            Console.WriteLine("Sum of all numbers is: {0}", sum);

        }

        static void FindAverage()
        {
            avg = sum / numbers.Count;
            Console.WriteLine("Average of all numbers is: {0}", avg);
        }

        static void FindStandardDeviation()
        {
            double sumSqDev = 0.0;
            double stDev;
            foreach (double num in numbers)
            {
                sumSqDev += (num - avg)*(num - avg);
            }
            stDev = Math.Sqrt(sumSqDev / (numbers.Count-1));
            Console.WriteLine("Standard deviation of all the numbers is: {0}", stDev);
        }

        static void Main(string[] args)
        {
            try
            {
                EnterValues();
                FindSmallestnumber();
                FindSumOfAllNumbers();
                FindAverage();
                FindStandardDeviation();
            }
            finally
            {
                Console.Write("Press any key to continue...");
                Console.ReadKey();
            }
            
        }
    }
}
