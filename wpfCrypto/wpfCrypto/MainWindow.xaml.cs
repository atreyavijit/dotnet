﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace wpfCrypto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string API_KEY = "";
        private const string FILE_PATH = @"../../../data.txt";

        private List<Crypto> lstCrypto = new List<Crypto>();

        public MainWindow()
        {
            InitializeComponent();
            if (File.Exists(FILE_PATH))
            {
                try
                {
                    using (StreamReader inputData = new StreamReader(FILE_PATH))
                    {
                        string line;
                        while ((line = inputData.ReadLine()) != null)
                        {
                            Crypto tmpCrypto = Crypto.CreateCryptoFromDataLine(line);
                            if(tmpCrypto != null)
                                lstCrypto.Add(tmpCrypto);
                        }
                    }
                }
                catch (IOException ex)
                {

                }
                finally
                {
                    lstCrypto = lstCrypto.OrderBy(cryptoItem => cryptoItem.Rank).ToList<Crypto>();
                    lvCryptos.ItemsSource = lstCrypto;
                    lvCryptos.Items.Refresh();

                    MessageBox.Show(String.Format("{0} Cryptocurrencies have been loaded.", lstCrypto.Count()));
                }
            }
            else
            {
                try
                {
                    string jsonStr = makeAPICall();
                    JObject jo = (JObject)JsonConvert.DeserializeObject(jsonStr);

                    JArray data = (JArray)jo["data"];
                    int length = data.Count;
                    CultureInfo provider = CultureInfo.InvariantCulture;

                    using (StreamWriter writer = new StreamWriter(@"../../../data.txt"))
                    {
                        for (int i = 0; i < length; i++)
                        {

                            string id = jo["data"][i]["id"].ToString();
                            string name = jo["data"][i]["name"].ToString();
                            string symbol = jo["data"][i]["symbol"].ToString();
                            string slug = jo["data"][i]["slug"].ToString();
                            string circulating_supply = jo["data"][i]["circulating_supply"].ToString();
                            string total_supply = jo["data"][i]["total_supply"].ToString();
                            string cmc_rank = jo["data"][i]["cmc_rank"].ToString();
                            //string date = jo["data"][i]["last_updated"].ToString();
                            var formatInfo = new DateTimeFormatInfo()
                            {
                                ShortDatePattern = "MM/dd/yyyy"
                            };
                            DateTime date = Convert.ToDateTime(jo["data"][i]["last_updated"].ToString(), formatInfo);

                            string price = jo["data"][i]["quote"]["USD"]["price"].ToString();
                            string volume_24h = jo["data"][i]["quote"]["USD"]["volume_24h"].ToString();
                            string market_cap = jo["data"][i]["quote"]["USD"]["market_cap"].ToString();
                            writer.WriteLine(String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9}", id, name, symbol, slug, circulating_supply, total_supply, cmc_rank, date.ToShortDateString(), price, volume_24h));
                            //writer.WriteLine(zone);\
                            Crypto tmpCrypto = new Crypto();

                        }
                        //writer.WriteLine(zone);

                        writer.Close();
                    }
                    //Console.WriteLine(jsonStr);
                    //Console.ReadKey();
                }
                catch (WebException e)
                {
                    MessageBox.Show(e.Message);

                }
            }
        }

        static string makeAPICall()
        {
            var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest");

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["start"] = "1";
            queryString["limit"] = "5000";
            queryString["convert"] = "USD";

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", API_KEY);
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());

        }


        public class Crypto
        {
            private int _id;
            private string _name;
            private string _symbol;
            private string _slug;
            private int _circulating_supply;
            private int _total_supply;
            private int _rank;
            
            private string _marketcap;
            private string _price;
            private double _volume24h;
            private double _change24h;

            public Crypto()
            {

            }
            // Factory Pattern
            public static Crypto CreateCryptoFromDataLine(string line) //DeSerialize
            {
                /*
                string slug = jo["data"][i]["slug"].ToString();
                string circulating_supply = jo["data"][i]["circulating_supply"].ToString();
                string total_supply = jo["data"][i]["total_supply"].ToString();
                string cmc_rank = jo["data"][i]["cmc_rank"].ToString();*/
                string[] tmpData = line.Split(';');
                if (!int.TryParse(tmpData[6], out int rank))
                {
                    return null;
                }
                if (!double.TryParse(tmpData[8], out double price))
                {
                    return null;
                }

                if (!double.TryParse(tmpData[9], out double marketCap))
                {
                    return null;
                }
                Crypto item = new Crypto();
                item.Rank = rank;

                item.Name = tmpData[1];
                item.Symbol = tmpData[2];
                item.Slug = tmpData[3];
                item.MarketCap = String.Format("${0:N2}", marketCap);
                item.Price = String.Format("${0:N2}", price);
                item.ID = Convert.ToInt32(tmpData[0]);

                return item;
            }
            public int ID
            {
                get { return _id; }
                set { _id = value; }
            }

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
            public string Symbol
            {
                get { return _symbol; }
                set { _symbol = value; }
            }
            public string Slug
            {
                get { return _slug; }
                set { _slug = value; }
            }

            public string MarketCap
            {
                get { return _marketcap; }
                set { _marketcap = value; }
            }
            public string Price
            {
                get { return _price; }
                set { _price = value; }
            }

            public int Rank
            {
                get { return _rank; }
                set { _rank = value; }
            }
            public double Volume24H
            {
                get { return _volume24h; }
                set { _volume24h = value; }
            }

            public double Change24H
            {
                get { return _change24h; }
                set { _change24h = value; }
            }
        }

    }


}