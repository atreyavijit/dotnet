﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02RandomWeather
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int temperature = rand.Next(-30,31);
            try
            {
                Console.WriteLine("Current temperature is: {0} degree celsius", temperature);
                if (temperature <= -15 && temperature >= -30)
                {
                    Console.WriteLine("Very very cold");
                }
                else if (temperature > -15 && temperature < 0)
                {
                    Console.WriteLine("Freezing already");
                }
                else if (temperature >= 0 && temperature <= 15)
                {
                    Console.WriteLine("Spring or Fall");
                }
                else if (temperature > 15 && temperature <= 30)
                {
                    Console.WriteLine("That's what I like");
                }
                else
                {
                    Console.WriteLine("Temperature is out of range");
                }
            }
            finally
            {
                Console.WriteLine("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
