﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02NamesAgain
{
    class Program
    {
        static int find(string name, string search)
        {
            int i, j, match, matchCount=0;
            for (i = 0; i < name.Length; i++)
            {
                match = 1;
                for (j = 0; j < search.Length; j++)
                {
                    if (i+j>=name.Length || name[i + j] != search[j])
                    {
                        match = 0;
                        break;
                    }
                }
                if (match == 1)
                {
                    matchCount++;
                    break;
                }
            }
            return matchCount;
        }
        static void Main(string[] args)
        {
            String[] nameList = new String[5];
            String search;
            int i, matchCount,longest;
            try
            {
                Console.WriteLine("Please provide 5 non-empty names.");
                for (i = 0; i < 5; i++)
                {
                    Console.Write("Enter Name{0}: ", i + 1);
                    nameList[i] = Console.ReadLine();
                    while (nameList[i] == "")
                    {
                        Console.Write("Name{0} is empty. Please enter a non empty name: ",i+1);
                        nameList[i] = Console.ReadLine();
                    }
                }
                Console.Write("\nNow enter a string to search: ");
                search = Console.ReadLine();
                while(search == "")
                {
                    Console.Write("You entered nothing to search. Please end a non empty string: ");
                    search = Console.ReadLine();
                }
                for (i = 0; i < 5; i++)
                {
                    matchCount = find(nameList[i], search);
                    if (matchCount>0)
                    {
                        Console.WriteLine("Matching name: {0}. Match count: {1}", nameList[i], matchCount);
                    }
                }
                longest = nameList[0].Length;
                for(i = 1; i < 5; i++)
                {
                    if (nameList[i].Length > longest)
                    {
                        longest = nameList[i].Length;
                    }
                }
                Console.WriteLine("\nLongest Name(s) is(are): ");
                for(i = 0; i < 5; i++)
                {
                    if(nameList[i].Length == longest)
                    {
                        Console.WriteLine("{0}. {1}", i + 1, nameList[i]);
                    }
                }

            }
            finally
            {
                Console.Write("Press any key to end the program");
                Console.ReadKey();
            }
        }
    }
}
