﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Class08TicTacToe
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int turnCount = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            Button[,] playButtons = new Button[3, 3];
            playButtons[0, 0] = btn00;
            playButtons[0, 1] = btn01;
            playButtons[0, 2] = btn02;
            playButtons[1, 0] = btn10;
            playButtons[1, 1] = btn11;
            playButtons[1, 2] = btn12;
            playButtons[2, 0] = btn20;
            playButtons[2, 1] = btn21;
            playButtons[2, 2] = btn22;
            string errorMessage = "";
            if (tbPlayer0.Text == "")
            {
                errorMessage += String.Format("Name for Player0 is empty{0}", Environment.NewLine);
                tbPlayer0.Background = Brushes.Red;
            }
            if (tbPlayerX.Text == "")
            {
                errorMessage += String.Format("Name for PlayerX is empty{0}", Environment.NewLine);
                tbPlayerX.Background = Brushes.Red;
            }
            if (errorMessage != "")
            {
                MessageBox.Show(errorMessage);
                return;
            }
            string player0 = tbPlayer0.Text;
            string playerX = tbPlayerX.Text;
            foreach (Button button in playButtons)
            {
                button.IsEnabled = true;
            }
            tbPlayer0.IsEnabled = false;
            tbPlayerX.IsEnabled = false;

        }
        private void Bt_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            if(btn.Content.ToString() != "")
            {
                return;
            }
            if (turnCount == 0)
            {
                btn.Content = "0";
                turnCount = 1;
            }
            else if (turnCount == 1)
            {
                btn.Content = "X";
                turnCount = 0;
            }
        }
    }
}
