﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07HotDogs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void PlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string name = tbName.Text;
                string DOB = tbDOB.Text;
                string errorMessage = "";
                if (!Regex.Match(name, @"^[^;]{1,}$").Success)
                {
                    errorMessage += "Name can't be empty and can't contain semicolon" + Environment.NewLine;
                }
                if (!Regex.Match(DOB, @"^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$").Success)
                {
                    errorMessage += "Date of birth can't be empty and must be in the format YYYY-MM-DD" + Environment.NewLine;
                }
                DateTime dob = Convert.ToDateTime(DOB);
                int age = DateTime.Now.Year - dob.Year;
                if(dob.DayOfYear > DateTime.Now.DayOfYear)
                {
                    age = age - 1;
                }
                if(age < 0 || age > 150)
                {
                    errorMessage += "Your age is " + age + ". You must be between 0 to 150 years"+Environment.NewLine;
                }
                if (errorMessage != "")
                {
                    MessageBox.Show(errorMessage);
                    return;
                }
                const string FILEPATH = @"../../../hotdogs.txt";
                string bunType = "";
                if (rbRegular.IsChecked == true)
                {
                    bunType = "Regular";
                }
                else if (rbWholeGrain.IsChecked == true)
                {
                    bunType = "Whole Grain";
                }
                else if (rbItalian.IsChecked == true)
                {
                    bunType = "Italian";
                }
                else
                {
                    MessageBox.Show("Internal Error");
                    return;
                }
                string sausageType = "";
                if (rbGerman.IsChecked == true)
                {
                    sausageType = "German";
                }
                else if (rbWithCheese.IsChecked == true)
                {
                    sausageType = "With Cheese";
                }
                else if (rbVegeterian.IsChecked == true)
                {
                    sausageType = "Vegeterian";
                }
                else
                {
                    MessageBox.Show("Internal Error");
                }
                double temperature = slTemperature.Value;
                string topings = "";
                if (cbKetchup.IsChecked == true)
                {
                    topings += "Ketchup,";
                }
                if (cbMustard.IsChecked == true)
                {
                    topings += "Mustard,";
                }
                if (cbRelish.IsChecked == true)
                {
                    topings += "Relish,";
                }
                string currentLine = "";
                if (topings.Length > 0)
                {
                    topings = topings.Substring(0, topings.Length - 1);
                    currentLine = string.Format("{0};{1},{2};{3};{4};{5}{6}", name, DOB, bunType, sausageType, temperature, topings, Environment.NewLine);

                }
                else
                {
                    currentLine = string.Format("{0};{1},{2};{3};{4}{5}", name, DOB, bunType, sausageType, temperature, Environment.NewLine);
                }
                File.AppendAllText(FILEPATH, currentLine);
                tbName.Text = "";
                tbDOB.Text = "";
                rbRegular.IsChecked = true;
                rbGerman.IsChecked = true;
                slTemperature.Value = 40;
                cbKetchup.IsChecked = false;
                cbMustard.IsChecked = false;
                cbRelish.IsChecked = false;
                MessageBox.Show("One record saved to file:"+Environment.NewLine+currentLine);
            }catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
