﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwners
{
    class Program
    {
        static ParkingContext ctx;
        //display menu
        static int ReadUserChoice()
        {
            int choice;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("***SELECT YOUR CHOICE***");
                Console.WriteLine("1. List all cars and their owner");
                Console.WriteLine("2. List all owners and their cars");
                Console.WriteLine("3. Add a car");
                Console.WriteLine("4. Add an owner");
                Console.WriteLine("5. Assign car to a new owner");
                Console.WriteLine("6. Delete an owner with all cars they own");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid Choice\n");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return choice;
        }

        //List all cars and their owners
        static void ListAllCarsAndTheirOwners()
        {
            List<Car> cars = (from c in ctx.Cars select c).ToList();
            if (cars.Count == 0)
            {
                Console.WriteLine("There are no cars in the database");
                return;
            }
            Console.WriteLine("List of existing cars along with thier owners:");
            foreach (var c in cars)
            {
                Console.WriteLine(c);
            }
        }

        //List all owners and thier cars
        static void ListAllOwnersAndThierCars()
        {
            List<Owner> owners = (from o in ctx.Owners select o).ToList();
            if (owners.Count == 0)
            {
                Console.WriteLine("There are no owners in the database");
                return;
            }
            Console.WriteLine("List of existing owners along with thier cars:");
            foreach (var o in owners)
            {
                Console.WriteLine(o);
            }
        }

        //add a car
        static void AddACar()
        {
            Console.Write("Enter make model of the car: ");
            string makeModel = Console.ReadLine();
            if (makeModel.Length < 1 || makeModel.Length > 100)
            {
                Console.WriteLine("Invalid make model. Must be between 1 to 100 characters.");
                return;
            }
            Console.Write("Enter year of production:");
            int yearOfProduction;
            if (!int.TryParse(Console.ReadLine(), out yearOfProduction))
            {
                Console.WriteLine("Invalid Year of Production. Must be an integer");
                return;
            }
            Car currentCar = new Car() { MakeModel = makeModel, YearOfProd = yearOfProduction };
            ctx.Cars.Add(currentCar);
            ctx.SaveChanges();
            Console.WriteLine("Car " + currentCar + " added successfully");
        }

        //Add an owner
        static void AddAnOwner()
        {
            Console.Write("Enter name of the Owner: ");
            string nameOfOwner = Console.ReadLine();
            if (nameOfOwner.Length < 1 && nameOfOwner.Length > 100)
            {
                Console.WriteLine("Invalid name. Must be between 1 to 100 characters.");
                return;
            }
            Owner currentOwner = new Owner() { Name = nameOfOwner };
            ctx.Owners.Add(currentOwner);
            ctx.SaveChanges();
            Console.WriteLine("Owner " + currentOwner + " added successfully");
        }

        //Assign a car to an owner
        static void AssignACarToAnOwner()
        {
            ListAllCarsAndTheirOwners();
            Console.Write("Enter car id to assign: ");
            int carId;
            if (!int.TryParse(Console.ReadLine(), out carId))
            {
                Console.WriteLine("Invalid car Id. Must be a integer");
                return;
            }

            ListAllOwnersAndThierCars();
            Console.Write("Enter owner id to assign: ");
            int ownerId;
            if (!int.TryParse(Console.ReadLine(), out ownerId))
            {
                Console.WriteLine("Invalid owner Id. Must be a integer");
                return;
            }
            var carList = (from c in ctx.Cars where c.Id == carId select c).ToList();
            var ownerList = (from o in ctx.Owners where o.Id == ownerId select o).ToList();

            Car currentCar = carList.Count == 0 ? null : carList[0];
            Owner currentOwner = ownerList.Count == 0 ? null : ownerList[0];

            if (currentCar == null && currentOwner == null)
            {
                Console.WriteLine("The car Id and the owner Id you specified don't exist");
                return;
            }
            else if (currentCar == null)
            {
                Console.WriteLine("The car Id you specified doesn't exist");
                return;
            }
            else if (currentOwner == null)
            {
                Console.WriteLine("The owner Id you specified doesn't exist");
                return;
            }
            else
            {
                currentCar.Owner = currentOwner;
                //currentOwner.CarsCollection.Add(currentCar);
                ctx.SaveChanges();
				Console.WriteLine("Car Id: {0} has been assigned to Owner Id: {1}", currentCar.Id, currentOwner.Id );
            }
        }

        //delete an owner with cars they own
        static void DeleteAnOwnerWithAllCars()
        {
            ListAllOwnersAndThierCars();
            Console.Write("Enter owner id to assign: ");
            int ownerId;
            if (!int.TryParse(Console.ReadLine(), out ownerId))
            {
                Console.WriteLine("Invalid owner Id. Must be a integer");
                return;
            }
            //var carList = (from c in ctx.Cars where c.Id == carId select c).ToList();
            var ownerList = (from o in ctx.Owners where o.Id == ownerId select o).ToList();

            //Car currentCar = carList.Count == 0 ? null : carList[0];
            Owner currentOwner = ownerList.Count == 0 ? null : ownerList[0];

            if (currentOwner == null)
            {
                Console.WriteLine("The owner Id you specified doesn't exist");
                return;
            }
            else
            {
                foreach(Car currentCar in currentOwner.CarsCollection.ToList())
                {
                    ctx.Cars.Remove(currentCar);
                }
                ctx.Owners.Remove(currentOwner);
                ctx.SaveChanges();
            }
        }

        //main method
        static void Main(string[] args)
        {
            try
            {
                ctx = new ParkingContext();
                int choice;
                do
                {
                    try
                    {
                        choice = ReadUserChoice();
                        switch (choice)
                        {
                            case 1:
                                ListAllCarsAndTheirOwners();
                                break;
                            case 2:
                                ListAllOwnersAndThierCars();
                                break;
                            case 3:
                                AddACar();
                                break;
                            case 4:
                                AddAnOwner();
                                break;
                            case 5:
                                AssignACarToAnOwner();
                                break;
                            case 6:
                                DeleteAnOwnerWithAllCars();
                                break;
                            case 0:
                                Console.WriteLine("Good Bye");
                                break;
                            default:
                                Console.WriteLine("Invalid Choice\n");
                                break;
                        }
                        if (choice == 0)
                        {
                            break;
                        }
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("Invalid data exception " + ex.Message);
                    }
                    catch (DataException ex)
                    {
                        Console.WriteLine("Data exception " + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Error: " + ex.Message);
                    }
                } while (true);
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}