﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwners
{
    public class Car
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string MakeModel { get; set; } // up to 100 characters

        public int YearOfProd { get; set; }

        public virtual Owner Owner { get; set; }

        public override string ToString()
        {
            if (Owner != null)
            {
                return String.Format("Car Id: {0}; Make Model: {1}; Year of Production: {2}; Owner: {3} (Owner Id: {4})", Id, MakeModel, YearOfProd, Owner.Name, Owner.Id);
            }
            else
            {
                return String.Format("Car Id: {0}; Make Model: {1}; Year of Production: {2}; Owner:  Not assigned", Id, MakeModel, YearOfProd);
            }
        }
    }
}