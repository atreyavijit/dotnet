﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day20CarsAndOwners
{
    public class Owner
    {
        public Owner()
        {
            CarsCollection = new HashSet<Car>(); //needs when you have a list of items in the principle
        }
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; } // up to 100 characters

        public virtual ICollection<Car> CarsCollection { get; set; }

        public override string ToString()
        {
            string carsOwned = "";
            if (CarsCollection.Count != 0)
            {
                foreach (Car car in CarsCollection)
                {
                    carsOwned += string.Format("{0} (Car Id {1}), ", car.MakeModel, car.Id);
                }
                if (carsOwned != "")
                {
                    carsOwned = carsOwned.Substring(0, carsOwned.Length - 1);
                }
            }
            else
            {
                carsOwned = "No cars owned yet";
            }
            return String.Format("Owner Id: {0}; Owner Name: {1}; Cars Owned: {2}", Id, Name, carsOwned);
        }
    }
}