﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Quiz2PeopleTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg)
        {

        }
    }

    class PersonTravels
    {
        public PersonTravels(string name, int age, string passportNo)
        {
            Name = name;
            Age = age;
            PassportNo = passportNo;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2 || value.Length > 50)
                {
                    throw new InvalidDataException("Name must be between 2 to 50 characters.");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value < 0 || value > 150)
                {
                    throw new InvalidDataException("Age must be between 0 to 150");
                }
                _age = value;
            }
        }
        private string _passportNo;
        public string PassportNo
        {
            get
            {
                return _passportNo;
            }
            set
            {
                if (!Regex.Match(value, "^[A-Z]{2}[0-9]{6}$").Success)
                {
                    throw new InvalidDataException("Invalid passport number");
                }
                _passportNo = value;
            }
        }
        public List<string> CountriesVisited = new List<string>();

        public override string ToString()
        {
            //Marianna is 29 y/o passport ZZ112233 visited: Japan, Nigeria, USA, Ukraine
            string countryString = "";
            foreach (string country in CountriesVisited)
            {
                countryString += country + ", ";
            }
            countryString = countryString.Substring(0, countryString.Length - 2); // to remove the last comma
            return string.Format("{0} is {1} y/o passpost {2} visited: {3}", Name, Age, PassportNo, countryString);
        }
    }
    class Program
    {
        //read data from file
        const string FILE_PATH = @"..\..\..\data.txt";
        static List<PersonTravels> personTravelsList = new List<PersonTravels>();
        static void ReadPeopleTravelsDataFromFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(FILE_PATH);
                foreach (string l in lines)
                {
                    string[] data = l.Split(';');
                    if (data.Length != 4)
                    {
                        Console.WriteLine("Corrupted line. Must have exactly four data: {0}", l);
                        continue;
                    }
                    string name = data[0];
                    int age;
                    if (!int.TryParse(data[1], out age))
                    {
                        Console.WriteLine("Corrupted line. Invalid age: {0}", l);
                        continue;
                    }
                    string passportNo = data[2];
                    try
                    {
                        PersonTravels p = new PersonTravels(name, age, passportNo);
                        if (data[3] == "")
                        {
                            Console.WriteLine("Corrupted line. Invalid data for countries visited: {0}", l);
                            continue;
                        }
                        string[] countryList = data[3].Split(',');
                        int corruptFlag = 0;
                        foreach (string country in countryList)
                        {
                            if (country == "")
                            {
                                Console.WriteLine("Corrupted line. Invalid data for countries visited {0}",l);
                                corruptFlag = 1;
                                continue;
                            }
                            p.CountriesVisited.Add(country);
                        }
                        if (corruptFlag == 0)
                        {
                            personTravelsList.Add(p);
                        }
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine("Corrupted line: {0}. {1}", ex.Message, l);
                    }
                }
            }
            catch (FileNotFoundException)
            {

            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: Can't read from the file: {0}", ex.Message);
            }
        }

        //writing final data to file
        static void SavePeopleTravelDataToFile()
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] FinalData = new String[personTravelsList.Count];
                int i = 0;
                foreach (PersonTravels person in personTravelsList)
                {
                    string countryString = "";
                    foreach (string country in person.CountriesVisited)
                    {
                        countryString += country + ",";
                    }
                    countryString = countryString.Substring(0, countryString.Length - 1); // to remove the last comma
                    FinalData[i++] = String.Format("{0};{1};{2};{3}", person.Name, person.Age, person.PassportNo, countryString);
                }
                File.WriteAllLines(FILE_PATH, FinalData);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to save data to file" + ex.Message);
            }
        }

        //displaying all info
        static void ShowAllPeopleAndCountries()
        {
            int i = 1;
            foreach (PersonTravels person in personTravelsList)
            {
                Console.Write("{0}. ", i++);
                Console.WriteLine(person);
            }
        }

        //adding a new person and country visited
        static void AddPersonAndCountriesVisited()
        {
            try
            {
                string name, passpostNo;
                int age;
                Console.WriteLine();
                Console.Write("Enter a name ");
                name = Console.ReadLine();
                Console.Write("Enter age: ");
                if (!int.TryParse(Console.ReadLine(), out age))
                {
                    throw new InvalidDataException("Invalid data for age");
                }
                Console.Write("Enter passport number: ");
                passpostNo = Console.ReadLine();
                personTravelsList.Add(new PersonTravels(name, age, passpostNo));
                Console.WriteLine("Countries Visited (one country per line):");
                do
                {
                    string country = Console.ReadLine();
                    if (country == "")
                    {
                        break;
                    }
                    personTravelsList[personTravelsList.Count - 1].CountriesVisited.Add(country);
                } while (true);
                Console.WriteLine("Person {0} added successfully:", name);
                Console.WriteLine(personTravelsList[personTravelsList.Count - 1]);
                Console.WriteLine();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message + ": Age must be a valid integer");
            }
        }

        //register a new country visited by user
        static void RegisterANewCountryVisitedByAPerson()
        {
            ShowAllPeopleAndCountries();
            int personNumber;
            Console.Write("Enter person number to add country to: ");
            if (!int.TryParse(Console.ReadLine(), out personNumber) || personNumber < 0 || personNumber > personTravelsList.Count)
            {
                throw new InvalidDataException("You provided invalid user number");
            }
            string countryName;
            Console.WriteLine("Enter country name: ");
            countryName = Console.ReadLine();
            personTravelsList[personNumber - 1].CountriesVisited.Add(countryName);
            Console.WriteLine("Country addedd successfully");
            Console.WriteLine(personTravelsList[personNumber - 1]);
            Console.WriteLine();
        }

        //who visited most countries
        static void WhoVisitedMostCountries()
        {
            int highestVisitCount = int.MinValue;
            foreach (PersonTravels person in personTravelsList)
            {
                if (person.CountriesVisited.Count > highestVisitCount)
                {
                    highestVisitCount = person.CountriesVisited.Count;
                }
            }
            Console.WriteLine("Highest number of countries visited: {0}. Person(s) who visited most countries is(are):", highestVisitCount);
            foreach (PersonTravels person in personTravelsList)
            {
                if (person.CountriesVisited.Count == highestVisitCount)
                {
                    Console.WriteLine(person);
                }
            }
        }

        //whi visited specific countries
        static void WhoVisitedSpecificCountries()
        {
            Console.WriteLine("Searching for all who visited a country");
            Console.WriteLine("Enter country name: ");
            string searchCountry = Console.ReadLine();
            if (searchCountry == "")
            {
                throw new InvalidDataException("Country name is empty");
            }
            Console.WriteLine("The following people visited {0}:", searchCountry);
            foreach (PersonTravels person in personTravelsList)
            {
                foreach (string country in person.CountriesVisited)
                {
                    if (searchCountry == country)
                    {
                        Console.WriteLine(person);
                    }
                }
            }
        }

        //reading user choice
        static int ReadUserChoice()
        {
            int choice;
            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("***SELECT YOUR CHOICE***");
                Console.WriteLine("1. Display all people and countries they traveled to");
                Console.WriteLine("2. Add person and countries visited");
                Console.WriteLine("3. Register a new country visited by a person");
                Console.WriteLine("4. Find out who visited most countries");
                Console.WriteLine("5. List all who visited a specific country");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid Choice\n");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return choice;
        }
        static void Main(string[] args)
        {
            try
            {
                int choice;
                ReadPeopleTravelsDataFromFile();
                do
                {
                    try
                    {
                        choice = ReadUserChoice();
                        switch (choice)
                        {
                            case 1:
                                ShowAllPeopleAndCountries();
                                break;
                            case 2:
                                AddPersonAndCountriesVisited();
                                break;
                            case 3:
                                RegisterANewCountryVisitedByAPerson();
                                break;
                            case 4:
                                WhoVisitedMostCountries();
                                break;
                            case 5:
                                WhoVisitedSpecificCountries();
                                break;
                            case 0:
                                Console.WriteLine("Good Bye");
                                break;
                            default:
                                Console.WriteLine("Invalid Choice\n");
                                break;
                        }
                        if (choice == 0)
                        {
                            break;
                        }
                    }
                    catch (InvalidDataException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (true);
                SavePeopleTravelDataToFile();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
