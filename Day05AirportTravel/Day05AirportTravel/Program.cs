﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day05AirportTravel
{
    class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg)
        {

        }
    }
    class Airport
    {
        public Airport(string code, string city, double latitude, double longitude)
        {
            Code = code;
            City = city;
            Latitude = latitude;
            Longitude = longitude;
        }
        private string _code;
        public string Code
        {
            get
            {
                return _code;
            }
            set
            {
                if (!Regex.Match(value, "^[A-Z]{3}$").Success)
                {
                    throw new InvalidDataException("Airport code must be three CAPITAL letter.");
                }
                _code = value;
            }
        }
        private string _city;
        public string City
        {
            get
            {
                return _city;
            }
            set
            {
                if (value == "")
                {
                    throw new InvalidDataException("City can't be empty");
                }
                _city = value;
            }
        }
        private double _latitude;
        public double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                if (value < -90.0 || value > 90.0)
                {
                    throw new InvalidDataException("Latitude must be between -90 to 90");
                }
                _latitude = value;
            }
        }
        private double _longitude;
        public double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                if (value < -180.0 || value > 180.0)
                {
                    throw new InvalidDataException("Longitude must be between -180 to 190");
                }
                _longitude = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Airport: {0} is at {1} having geographical location ({2},{3})", Code, City, Latitude, Longitude);
        }
    }
    class Program
    {
        const string FILE_PATH = @"..\..\..\airports.txt";
        static List<Airport> airportList = new List<Airport>();

        //reading airport data from file
        static void ReadAirportsFromFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(FILE_PATH);
                foreach (string l in lines)
                {
                    string[] data = l.Split(';');
                    if (data.Length != 4)
                    {
                        Console.WriteLine("Corrupted line. Must have exactly four data: {0}", l);
                        continue;
                    }
                    string code = data[0];
                    string city = data[1];
                    double latitude;
                    if (!double.TryParse(data[2], out latitude))
                    {
                        Console.WriteLine("Corrupted line. Invalid latitude: {0}", l);
                        continue;
                    }
                    double longitude;
                    if (!double.TryParse(data[3], out longitude))
                    {
                        Console.WriteLine("Corrupted line. Invalid longitude: {0}", l);
                        continue;
                    }
                    try
                    {
                        Airport a = new Airport(code,city,latitude,longitude);
                        airportList.Add(a);
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        Console.WriteLine("Corrupted line: {0}\n{1}", l, ex.Message);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                //do nothing
                //its okay to start this program without the file
                //you can read data which will ultimately be written to file at the end
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: Can't read from the file: {0}", ex.Message);
            }
        }

        //writing the final airport data to the file
        static void SaveAirportDataToFile()
        {
            try //trying to catch exceptions like insufficient disk space
            {
                String[] FinalData = new String[airportList.Count];
                int i = 0;
                foreach (Airport a in airportList)
                {
                    FinalData[i++] = String.Format("{0};{1};{2};{3}", a.Code, a.City, a.Latitude, a.Longitude);
                }
                File.WriteAllLines(FILE_PATH, FinalData);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: failed to save data to file" + ex.Message);
            }
        }

        //function to calculate distance between two locations
        public static double CalculateDistance(double Lat1, double Long1, double Lat2, double Long2)
        {
            /*
                The Haversine formula according to Dr. Math.
                http://mathforum.org/library/drmath/view/51879.html

                dlon = lon2 - lon1
                dlat = lat2 - lat1
                a = (sin(dlat/2))^2 + cos(lat1) * cos(lat2) * (sin(dlon/2))^2
                c = 2 * atan2(sqrt(a), sqrt(1-a)) 
                d = R * c

                Where
                    * dlon is the change in longitude
                    * dlat is the change in latitude
                    * c is the great circle distance in Radians.
                    * R is the radius of a spherical Earth.
                    * The locations of the two points in 
                        spherical coordinates (longitude and 
                        latitude) are lon1,lat1 and lon2, lat2.
            */
            double dDistance = Double.MinValue;
            double dLat1InRad = Lat1 * (Math.PI / 180.0);
            double dLong1InRad = Long1 * (Math.PI / 180.0);
            double dLat2InRad = Lat2 * (Math.PI / 180.0);
            double dLong2InRad = Long2 * (Math.PI / 180.0);

            double dLongitude = dLong2InRad - dLong1InRad;
            double dLatitude = dLat2InRad - dLat1InRad;

            // Intermediate result a.
            double a = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                       Math.Cos(dLat1InRad) * Math.Cos(dLat2InRad) *
                       Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            // Intermediate result c (great circle distance in Radians).
            double c = 2.0 * Math.Asin(Math.Sqrt(a));

            // Distance.
            // const Double kEarthRadiusMiles = 3956.0;
            const Double kEarthRadiusKms = 6376.5;
            dDistance = kEarthRadiusKms * c;

            return dDistance;
        }

        //Show all airports function
        static void ShowAllAirports()
        {
            Console.WriteLine();
            Console.WriteLine("Listing all Airports");
            foreach (Airport a in airportList)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine("");
        }

        //find distance between two airports
        static void FindDistance()
        {
            string source, destination;
            double distance;
            Console.WriteLine();
            Console.Write("Enter a valid source airport code: ");
            source = Console.ReadLine();
            Airport src = AirportExist(source);
            if(src == null)
            {
                Console.WriteLine("The source airport code you have provided doesn't exist");
                return;
            }
            Console.Write("Enter a valid destination airport code: ");
            destination = Console.ReadLine();
            Airport dest = AirportExist(destination);
            if (dest == null)
            {
                Console.WriteLine("The destination airport code you have provided doesn't exist");
                return;
            }
            distance = CalculateDistance(src.Latitude,src.Longitude,dest.Latitude,dest.Longitude);
            Console.WriteLine("The distance is: {0} kilometers", Math.Round(distance,3));
        }

        static void FindNearestAirport()
        {
            string current;
            Console.WriteLine();
            Console.WriteLine("Enter your current airport code: ");
            current = Console.ReadLine();
            Airport currentAirport = AirportExist(current);
            if(currentAirport == null)
            {
                Console.WriteLine("The airport code you have provided doesn't exist");
                return;
            }
            //double minDistance = CalculateDistance(currentAirport.Latitude, currentAirport.Longitude, airportList[0].Latitude, airportList[0].Longitude);
            double minDistance = double.MaxValue;
            Airport nearestAirport = null;
            foreach (Airport a in airportList)
            {
                if(a == currentAirport)
                {
                    continue;
                }
                double newDistance = CalculateDistance(currentAirport.Latitude, currentAirport.Longitude, a.Latitude, a.Longitude);
                if (newDistance < minDistance)
                {
                    minDistance = newDistance;
                    nearestAirport = a;
                }
            }
            Console.WriteLine("The nearest airport from {0} is {1} at {2:0.000} Kilometers", current, nearestAirport.Code, minDistance);
        }

        //add a new airport to the list
        static void AddNewAirport()
        {

            try
            {
                string code, city;
                double latitude, longitude;
                Console.WriteLine();
                Console.Write("Enter airport code (3 letter all uppercase): ");
                code = Console.ReadLine();
                Console.Write("Enter a city: ");
                city = Console.ReadLine();
                Console.Write("Enter latitude (must be from -90.0 to 90.0): ");
                double.TryParse(Console.ReadLine(), out latitude);
                Console.Write("Enter longitude (must be from -180.0 to 180.0): ");
                double.TryParse(Console.ReadLine(), out longitude);
                airportList.Add(new Airport(code, city, latitude, longitude));
                Console.WriteLine("New Airport {0} added", code);
                Console.WriteLine();
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Console.WriteLine("Error : " + ex.Message);
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message + ": Latitude/Longitude must be a floating point number");
            }
        }

            //to check if an airport exists or not
        static Airport AirportExist(string airportCode)
        {
            foreach (Airport a in airportList)
            {
                if (a.Code == airportCode)
                {
                    return a;
                }
            }
            return null;
        }

        //reading user choice
        static int ReadUserChoice()
        {
            int choice;
            while (true)
            {
                Console.WriteLine("***SELECT YOUR CHOICE***");
                Console.WriteLine("1. Show all airports");
                Console.WriteLine("2. Find distance between two airports by codes");
                Console.WriteLine("3. Find the nearest airports");
                Console.WriteLine("4. Add a new airport");
                Console.WriteLine("0. Exit");
                Console.Write("Choice: ");
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    Console.WriteLine("Invalid Choice\n");
                    continue;
                }
                else
                {
                    break;
                }
            }
            return choice;
        }

        //main function
        static void Main(string[] args)
        {
            try
            {
                int choice;
                ReadAirportsFromFile();
                do
                {
                    choice = ReadUserChoice();
                    switch (choice)
                    {
                        case 1:
                            ShowAllAirports();
                            break;
                        case 2:
                            FindDistance();
                            break;
                        case 3:
                            FindNearestAirport();
                            break;
                        case 4:
                            AddNewAirport();
                            break;
                        case 0:
                            Console.WriteLine("Good Bye");
                            break;
                        default:
                            Console.WriteLine("Invalid Choice\n");
                            break;
                    }
                } while (choice != 0);
                SaveAirportDataToFile();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
