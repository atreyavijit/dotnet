﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatesSeries
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime curDate;
            curDate = DateTime.Now;
            for (int i = 0; i < 365; i++)
            {
                Console.WriteLine(curDate.ToString("yyyy-MM-dd"));
                curDate = curDate.AddDays(-1);
            }
            Console.ReadKey();

        }
    }
}
