﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day02Tree
{
    class Program
    {
        static void Main(string[] args)
        {
            string depthStr;
            int depth;
            try
            {
                Console.Write("How big does your tree needs to be?");
                depthStr = Console.ReadLine();
                while (!int.TryParse(depthStr, out depth))
                {
                    Console.Write("Invalid depth. Please provide an integer: ");
                    depthStr = Console.ReadLine();
                }
                for (int i = 1; i <= depth; i++)
                {
                    for (int j = 1; j <= depth + i - 1; j++)
                    {
                        if (i + j <= depth)
                        {
                            Console.Write(" ");
                        }
                        else
                        {
                            Console.Write("*");
                        }
                    }
                    Console.WriteLine();
                }
            }
            finally
            {
                Console.Write("Press any key to continue ...");
                Console.ReadKey();
            }

        }
    }
}
