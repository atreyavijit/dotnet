﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day05ManyPeople
{
    //class Person
    class Person
    {
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length < 2)
                {
                    throw new ArgumentOutOfRangeException("Name must be at least 2 characters long");
                }
                _name = value;
            }
        }
        private int _age;
        public int Age
        {
            get
            {
                return _age;
            }
            set
            {
                if (value > 150 || value < 0)
                {
                    throw new ArgumentOutOfRangeException("Age must be between 0 and 150");
                }
                _age = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Person {0} is {1} years", Name, Age);
        }
    }

    //class Student
    class Student : Person
    {
        public Student(string name, int age, string program, double gpa) : base(name, age)
        {
            Program = program;
            GPA = gpa;
        }
        private string _program;
        public string Program
        {
            get
            {
                return _program;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Program  must be between 2 to 100 characters");
                }
                _program = value;
            }
        }
        private double _gpa;
        public double GPA
        {
            get
            {
                return _gpa;
            }
            set
            {
                if (value > 4.0 || value < 0.0)
                {
                    throw new ArgumentOutOfRangeException("GPA must be between 0.0 to 4.0");
                }
                _gpa = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Student {0} is {1} years studying {2} with GPA {3}", Name, Age, Program, GPA);
        }
    }

    //class Teacher
    class Teacher : Person
    {
        public Teacher(string name, int age, string field, int yoe) : base(name, age)
        {
            Field = field;
            YOE = yoe;
        }
        private string _field;
        public string Field
        {
            get
            {
                return _field;
            }
            set
            {
                if (value.Length < 2 || value.Length > 100)
                {
                    throw new ArgumentOutOfRangeException("Field must be between 2 to 100 characters");
                }
                _field = value;
            }
        }
        private int _yoe;
        public int YOE
        {
            get
            {
                return _yoe;
            }
            set
            {
                if (value > 100 || value < 0)
                {
                    throw new ArgumentOutOfRangeException("Years of Experience must be between 0 and 150");
                }
                _yoe = value;
            }
        }
        public override string ToString()
        {
            return string.Format("Teacher {0} is {1} years expert in {2} with {3} years of experience", Name, Age, Field, YOE);
        }
    }
    class Program
    {
        static List<Person> people = new List<Person>();
        const string FILE_PATH = @"..\..\..\people.txt";
        static void ReadPeopleFromFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(FILE_PATH);
                foreach (string l in lines)
                {
                    try
                    {
                        string[] data = l.Split(';');
                        if (data.Length != 3 && data.Length != 5)
                        {
                            Console.WriteLine("Corrupted line. Must have either three or five data: {0}", l);
                            continue;
                        }
                        string type = data[0];
                        if ((type != "Person") && (type != "Student") && (type != "Teacher"))
                        {
                            Console.WriteLine("Corrupted line. Type is invalid: {0}", l);
                            continue;
                        }
                        string name = data[1];
                        if (name == "")
                        {
                            Console.WriteLine("Corrupted line. Name is empty: {0}", l);
                            continue;
                        }
                        int age;
                        if (!int.TryParse(data[2], out age))
                        {
                            Console.WriteLine("Corrupted line. Invalid age: {0}", l);
                            continue;
                        }
                        if (data[0] == "Student")
                        {
                            string program = data[3];
                            if (program == "")
                            {
                                Console.WriteLine("Corrupted line. Empty program: {0}", l);
                                continue;
                            }
                            double gpa;
                            if (!double.TryParse(data[4], out gpa))
                            {
                                Console.WriteLine("Corrupted line. Invalid GPA: {0}", l);
                                continue;
                            }
                            Student s = new Student(name, age, program, gpa);
                            people.Add(s);
                        }
                        else if (data[0] == "Teacher")
                        {
                            string field = data[3];
                            if (field == "")
                            {
                                Console.WriteLine("Corrupted line. Empty field: {0}", l);
                                continue;
                            }
                            int yoe;
                            if (!int.TryParse(data[4], out yoe))
                            {
                                Console.WriteLine("Corrupted line. Invalid data for Years of Experience: {0}", l);
                                continue;
                            }
                            Teacher t = new Teacher(name, age, field, yoe);
                            people.Add(t);
                        }
                        else
                        {
                            Person p = new Person(name, age);
                            people.Add(p);
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        Console.WriteLine("Corrupted line: {0}\t{1}", l, ex.Message);
                    }
                }
            }
            catch (FileNotFoundException)
            {
                //do nothing
                //its okay to start this program without the file
                //you can read data which will ultimately be written to file at the end
            }
            catch (IOException ex)
            {
                Console.WriteLine("Error: Can't read from the file: {0}", ex.Message);
            }
        }
        static void Main(string[] args)
        {
            try
            {
                ReadPeopleFromFile();
                Console.WriteLine();
                foreach (Person p in people)
                {
                    Console.WriteLine(p);
                }
                Console.WriteLine("\n******ONLY STUDENTS******");
                foreach (Person p in people)
                {
                    if (p is Student)
                    {
                        Console.WriteLine(p);
                    }
                }

                Console.WriteLine("\n******ONLY TEACHERS******");
                foreach (Person p in people)
                {
                    if (p is Teacher)
                    {
                        Console.WriteLine(p);
                    }
                }
                Console.WriteLine("\n******ONLY PERSONS******");
                foreach (Person p in people)
                {
                    if (p.GetType() == typeof(Person))
                    {
                        Console.WriteLine(p);
                    }
                }
            }
            finally
            {
                Console.Write("Press any key to end");
                Console.ReadKey();
            }
        }
    }
}
