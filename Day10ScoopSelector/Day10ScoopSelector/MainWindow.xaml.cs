﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day10ScoopSelector
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            ListViewItem item = lvFlavours.SelectedItem as ListViewItem; //typecasting
            if(item == null)
            {
                MessageBox.Show("Select a falvour first!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            lvSelected.Items.Add(item.Content);
        }

        private void BtnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lvSelected.Items.Count == 0)
            {
                MessageBox.Show("Your list is empty!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            else if (lvSelected.SelectedItem == null)
            {
                MessageBox.Show("No item selected!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            lvSelected.Items.Remove(lvSelected.SelectedItem);
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            if (lvSelected.Items.Count == 0)
            {
                MessageBox.Show("Your list is empty!", "Scoop Selector", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            lvSelected.Items.Clear();
        }
    }
}
