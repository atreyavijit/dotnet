﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz1Library
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public class Book
    {
        public Book() { }
        public Book(string dataLine)
        {
            string[] data = dataLine.Split(';');
            if (data.Length != 5)
            {
                throw new InvalidDataException("Data line must be composed of 5 items exactly\n"+dataLine);
            }
            if (!Regex.Match(data[0], @"^[^;]{1,100}$").Success || !Regex.Match(data[1], @"^[^;]{1,100}$").Success)
            {
                throw new InvalidDataException("Author name and title must be 1-100 characters long, no semicolons\n" + dataLine);
            }
            Author = data[0];
            Title = data[1];
            Genre = data[2];
            
            try
            {
                Price = double.Parse(data[3]);
                PubDate = DateTime.ParseExact(data[4], "yyyy-MM-dd", null);
            }
            catch (FormatException ex)
            {
                // exception chaining - translating one exception into another
                throw new InvalidDataException("Data format invalid", ex);
            }
        } 
        public string Author, Title; // 1-100 characters, no semicolons
        public string Genre; // drop-box: Romance, Self-Help, Other
        public double Price; // 0-10000, display with 2 decimal places
        public DateTime PubDate; // publication date - DatePicker
        public override string ToString()
        {
            return string.Format("{0} by {1} is {2} (${3:0.00}) published on {4}", Title, Author, Genre, Price, PubDate.ToShortDateString());
        }
        public string ToDataString()
        {
            return string.Format("{0};{1};{2};{3};{4}", Author,Title,Genre, Price, PubDate.ToString("yyyy-MM-dd"));
        }
    }
    public class DataInvalidException : Exception
    {
        public DataInvalidException(String msg) : base(msg) { }
        public DataInvalidException(String msg, Exception ex) : base(msg, ex) { }
    }

    public partial class MainWindow : Window
    {
        public const string FILE_NAME = @"..\..\..\library.dat";

        List<Book> bookList = new List<Book>();
        public MainWindow()
        {
            InitializeComponent();
            LoadDataFromFile(FILE_NAME);
            lvBookList.ItemsSource = bookList;
        }

        //load data from the file
        public void LoadDataFromFile(string filePath)
        {
            try
            {
                string[] linesList = File.ReadAllLines(filePath);
                foreach (string line in linesList)
                {
                    try
                    {
                        bookList.Add(new Book(line));
                    }
                    catch (InvalidDataException ex)
                    {
                        MessageBox.Show("Error reading line from file: " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                //the program runs even without the file 
            } 
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //save data to file
        void SaveDataToFile(string filePath, List<Book> dataList)
        {
            List<string> linesList = new List<string>();
            try
            {
                foreach (Book book in dataList)
                {
                    linesList.Add(book.ToDataString());
                }
                File.WriteAllLines(filePath, linesList);
                sbLastOp.Text = string.Format("{0} data has been written to {1}", dataList.Count, filePath);
            }
            catch (IOException ex)
            {
                MessageBox.Show("Error saving to file: " + ex.Message, "Todo List", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileExportSelected_MenuClick(object sender, RoutedEventArgs e)
        {
            if (lvBookList.SelectedItems.Count == 0)
            {
                MessageBox.Show("You must select one or more items for export first", "Library", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "Books Files|*.books|All files|*.*";
            dialog.Title = "Export Books";
            dialog.ShowDialog();
            if (dialog.FileName != "")
            {
                try
                {
                    List<Book> selectedBooksList = new List<Book>();
                    foreach (Book book in lvBookList.SelectedItems)
                    {
                        selectedBooksList.Add(book);
                    }
                    SaveDataToFile(dialog.FileName, selectedBooksList);
                }
                catch (IOException ex)
                {
                    MessageBox.Show("Error saving to file: " + ex.Message, "Library", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            SaveDataToFile(FILE_NAME, bookList);
        }

        private void LvBookList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Book book = lvBookList.SelectedItem as Book;
            if (book == null)
            { 
                btUpdateBook.IsEnabled = false;
                btDeleteBook.IsEnabled = false;
                return;
            }
            btUpdateBook.IsEnabled = true;
            btDeleteBook.IsEnabled = true;
            // load data of currently selected item (first selected)
            tbAuthor.Text = book.Author;
            tbTitle.Text = book.Title;
            cbGenre.Text = book.Genre;
            slBookPrice.Value = book.Price;
            dpDatePublished.SelectedDate = book.PubDate;
        }

        private void ButtonAddUpdateBook_Click(object sender, RoutedEventArgs e)
        {
            string author = tbAuthor.Text;
            string title = tbTitle.Text;
            string genre = cbGenre.Text;
            double price = slBookPrice.Value;
            if (dpDatePublished.SelectedDate == null)
            {
                MessageBox.Show("Please choose the published date", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            DateTime pubDate = dpDatePublished.SelectedDate.Value;
            if (!Regex.Match(author, @"^[^;]{1,100}$").Success || !Regex.Match(title, @"^[^;]{1,100}$").Success)
            { 
                MessageBox.Show("Author name and title must be 1-100 characters long, no semicolons", "Library", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            Button senderButton = sender as Button;
            if (senderButton.Name == "btUpdateBook")
            {
                Book book = lvBookList.SelectedItem as Book;
                book.Author = author;
                book.Title = title;
                book.Genre = genre;
                book.Price = price;
                book.PubDate = pubDate;
            }
            else
            {
                Book book = new Book()
                    {
                        Author = author,
                        Title = title,
                        Genre = genre,
                        Price = price,
                        PubDate = pubDate
                    };
                bookList.Add(book);
                // cleanup
                tbAuthor.Text = "";
                tbTitle.Text = "";
                cbGenre.Text = "";
                slBookPrice.Value = 100;

            }
            lvBookList.Items.Refresh();
        }

        private void ButtonDeleteTask_Click(object sender, RoutedEventArgs e)
        {
            Book book = lvBookList.SelectedItem as Book;
            MessageBoxResult result = MessageBox.Show("Do you really want to delete the book:" + Environment.NewLine + book.ToString(), "Library", MessageBoxButton.OKCancel, MessageBoxImage.Question, MessageBoxResult.Cancel);
            if (result == MessageBoxResult.OK)
            {
                bookList.Remove(book);
                lvBookList.Items.Refresh();
                tbAuthor.Text = "";
                tbTitle.Text = "";
                cbGenre.Text = "";
                slBookPrice.Value = 100;
            }
        }

        private void TbBookPrice_TextChanged(object sender, TextChangedEventArgs e)
        {
            double price;
            if (!double.TryParse(tbBookPrice.Text, out price) && tbBookPrice.Text != "")
            {
                MessageBox.Show("Price must be a number");
                return;
            }
            if (price < 0 || price > 1000 || tbBookPrice.Text == "")
            {
                price = 0;
            }
            slBookPrice.Value = price;
        }

        private void TbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            List<Book> searchList = bookList.Where(t => t.Author.Contains(tbSearch.Text) || t.Title.Contains(tbSearch.Text)).ToList();
            lvBookList.ItemsSource = searchList;
        }
    }
}
