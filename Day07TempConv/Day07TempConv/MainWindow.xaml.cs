﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Day07TempConv
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} C", double.Parse(tbInput.Text));
                }
                if(rbOutputF.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} F", 9 * double.Parse(tbInput.Text) / 5 + 32);
                }
                if(rbOutputK.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} K", double.Parse(tbInput.Text) + 273.15);
                }
            }
            if (rbInputF.IsChecked == true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} C", 5 * (double.Parse(tbInput.Text) - 32) / 9);
                }
                if (rbOutputF.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} F", double.Parse(tbInput.Text));
                }
                if (rbOutputK.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} K", 5 * (double.Parse(tbInput.Text) - 32) / 9 + 273.15);
                }
            }
            if (rbInputK.IsChecked == true)
            {
                if (rbOutputC.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} C", double.Parse(tbInput.Text) - 273.15);
                }
                if (rbOutputF.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} F", 9 * (double.Parse(tbInput.Text) - 273.15) / 5 + 32);
                }
                if (rbOutputK.IsChecked == true)
                {
                    lbResult.Content = String.Format("{0:0.##} K", double.Parse(tbInput.Text));
                }
            }
        }
    }
}
